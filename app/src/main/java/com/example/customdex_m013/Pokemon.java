package com.example.customdex_m013;






import java.io.Serializable;
import java.util.List;

public class Pokemon{
    public Sprites sprites;
    public String name;
    public Integer id;
    public static int comparePokes(Pokemon a, Pokemon b){
        return a.id.compareTo(b.id);
    }
    public List<SlotType> types;

    public List<Abilities> abilities;
    public int base_experience;
    public List<Held_item>held_items;
    public int height;
    public int weight;
    public List<Moves> moves;
    public Species species;
    public List<Stats> stats;

    public Pokemon() {
    }

    public Pokemon(Sprites sprites, String name) {
        this.sprites = sprites;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    }
    /* public Pokemon(Sprites sprites, String name, List<SlotType> types, List<Abilities> abilities, int base_experience, List<Held_item> held_items, int height, int weight, Integer id, List<Moves> moves, List<Species> species, List<Stats> stats) {
        this.sprites = sprites;
        this.name = name;
        this.types = types;
        this.abilities = abilities;
        this.base_experience = base_experience;
        this.held_items = held_items;
        this.height = height;
        this.weight = weight;
        this.id = id;
        this.moves = moves;
        this.species = species;
        this.stats = stats;
    }*/

    /*@Override
    public String toString() {
        return "Pokemon{" +
                "sprites=" + sprites +
                ", name='" + name + '\'' +
                ", types=" + types +
                ", abilities=" + abilities +
                ", base_experience=" + base_experience +
                ", held_items=" + held_items +
                ", height=" + height +
                ", weight=" + weight +
                ", id=" + id +
                ", moves=" + moves +
                ", species=" + species +
                ", stats=" + stats +
                '}';
    }*/

class Type{
    public String name;
    public String url;



}
class SlotType implements Serializable {
    public int slot;
    public Type type;
}
class Sprites{
    public String front_default;
    public String front_shiny;
    public String front_female;
    public String front_shiny_female;
    public String back_default;
    public String back_shiny;
    public String back_female;
    public String back_shiny_female;
    @Override
    public String toString() {
        return "Sprite{" +
                "front_default='" + front_default + '\'' +
                ", front_shiny='" + front_shiny + '\'' +
                ", front_female='" + front_female + '\'' +
                ", front_shiny_female='" + front_shiny_female + '\'' +
                ", back_default='" + back_default + '\'' +
                ", back_shiny='" + back_shiny + '\'' +
                ", back_female='" + back_female + '\'' +
                ", back_shiny_female='" + back_shiny_female + '\'' +
                '}';
    }
}

class Ability{
    public String name;
    public String url;
}
class Abilities{
    public Ability ability;
    public boolean is_hidden;
    public int slot;
}
class Item{
    public String name;
    public String url;
}
class Held_item{
    public Item item;
    public List<Version_details> version_details;
}
class Version_details{
    public int rarity;
    public Version version;
}
class Version{
    public String name;
    public String url;
}

class Moves{
    public Move move;
    public List<Version_group_details> version_group_details;
}
class Move{
    public String name;
    public String url;
}
class Version_group_details{
    public int level_learned_at;
    public Move_learn_method move_learn_method;
    public Version_group version_group;
}
class Move_learn_method{
    public String name;
    public String url;
}
class Version_group{
    public String name;
    public String url;
}
class Species{
    public String name;
    public String url;
}
class Stats{
    public int base_stat;
    public int effort;
    public Stat stat;
}
class Stat{
    public String name;
    public String url;
}


