package com.example.customdex_m013;

import java.util.List;

public class EvolutionChain {
    Baby_trigger_item baby_trigger_item;
    Chain chain;
    int id;

    public EvolutionChain() {
    }
}
class Baby_trigger_item{
    String name;
    String url;
}
class Chain{
    boolean is_baby;
    List<Evolves_to> evolves_to;
    Species species;
}
class Evolves_to{
    Species species;
    List<Evolution_Details> evolution_details;
    List<Evolves_to> evolves_to;
    boolean is_baby;
}
class Evolution_Details{
    int gender;
    int min_beauty;
    int min_happiness;
    Item item;
    Heldo_item held_item;
    boolean needs_overworld_rain;
    int min_level;
    Known_move known_move;
    Trigger trigger;
    Location location;
    String time_of_day;
    Known_move_type known_move_type;
    Trade_species trade_species;
    Party_species party_species;
    Party_type party_type;
    int relative_physical_stats;
    boolean turn_uspide_down;

}

class Heldo_item{
    String name;
    String url;
}
class Known_move{
    String name;
    String url;
}
class Trigger{
    String name;
    String url;
}
class Location{
    String name, url;
}
class Known_move_type{
    String name, url;
}
class Trade_species{
    String name, url;
}
class Party_species{
    String name, url;
}
class Party_type{
    String name, url;
}


