package com.example.customdex_m013;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.Toast;


import com.example.customdex_m013.databinding.ActivityForumBinding;
import com.example.customdex_m013.databinding.ForumRowBinding;
import com.example.customdex_m013.model.Comment;
import com.example.customdex_m013.model.Forum;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ForumActivity extends AppCompatActivity {

    private ActivityForumBinding binding;
    private ArrayList<Forum> forumList = new ArrayList<>();
    private ForumAdapter adapter;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivityForumBinding.inflate(getLayoutInflater())).getRoot());

        binding.addForum.setOnClickListener(addForum -> {
            startActivity(new Intent(this, ForumAddActivity.class));
        });

        binding.recyclerView.setAdapter(adapter = new ForumAdapter());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));

        setQuery().addSnapshotListener((collectionSnapshot, e) -> {
            forumList.clear();
            for (DocumentSnapshot documentSnapshot: collectionSnapshot) {
                Forum forum = documentSnapshot.toObject(Forum.class);
                forum.forumId = documentSnapshot.getId();
                forumList.add(forum);
                if(!forumList.isEmpty()) Collections.sort(forumList, new SortForumByDate());

            }
            adapter.notifyDataSetChanged();
        });

        binding.buscarForo.setOnClickListener(buscarForo -> {
            //forumList.clear();
            setQuery().addSnapshotListener((collectionSnapshot, e) -> {
                forumList.clear();
                for (DocumentSnapshot documentSnapshot: collectionSnapshot) {
                    Forum forum = documentSnapshot.toObject(Forum.class);
                    forum.forumId = documentSnapshot.getId();
                    if (forum.premise.contains(binding.foroAbuscar.getText().toString())) forumList.add(forum);

                    if(!forumList.isEmpty()) Collections.sort(forumList, new SortForumByDate());
                }
                adapter.notifyDataSetChanged();
            });
            binding.foroAbuscar.setText("");
        });
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

    }

    Query setQuery() { return db.collection("forums"); }

    class ForumAdapter extends RecyclerView.Adapter<ForumAdapter.ViewHolder> {

        @NonNull
        @Override
        public ForumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(ForumRowBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ForumAdapter.ViewHolder holder, int position) {
            Forum forum = forumList.get(position);
            holder.binding.premisa.setText(forum.premise);
            Picasso.get().load(forum.url).fit().centerCrop().into(holder.binding.profilepic);
            holder.binding.name.setText(forum.authorName);
            holder.binding.forumLayout.setOnClickListener(click -> {
                Intent detail = new Intent(getApplicationContext(), ForumDetailActivity.class);
                detail.putExtra("premise", forumList.get(holder.getAdapterPosition()).premise);
                detail.putExtra("url", forumList.get(holder.getAdapterPosition()).url);
                detail.putExtra("authorName", forumList.get(holder.getAdapterPosition()).authorName);
                detail.putExtra("forumId", forumList.get(holder.getAdapterPosition()).forumId);
                startActivity(detail);
            });
        }

        @Override
        public int getItemCount() {
            return forumList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            ForumRowBinding binding;
            public ViewHolder(ForumRowBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
    static class SortForumByDate implements Comparator<Forum> {

        @Override
        public int compare(Forum forum, Forum forum2) {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            try {
                return format.parse(forum.date).compareTo(format.parse(forum2.date));
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
            //return forum.date.compareTo(forum2.date);
        }

        @Override
        public boolean equals(Object o) {
            return false;
        }
    }
}