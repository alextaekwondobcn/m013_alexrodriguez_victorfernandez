package com.example.customdex_m013;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.customdex_m013.databinding.FragmentHomeBinding;
import com.example.customdex_m013.model.PokemonCustom;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HomeFragment extends Fragment {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private FirebaseAuth auth = FirebaseAuth.getInstance();
    private FragmentHomeBinding binding;
    private ArrayList<PokemonCustom> pokemonCustoms = new ArrayList<>();
    private ArrayList<PokemonCustom> pokemonCustoms2 = new ArrayList<>();
    private MediaAdapter adapter;
    private MediaAdapter2 adapter2;
    int counter = 0;
    int counter2 = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return (binding = FragmentHomeBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.buttonForos.setOnClickListener(foros -> {
            startActivity(new Intent(getContext(), ForumActivity.class));
        });

        setQuery().addSnapshotListener((collectionSnapshot, e) -> {
            pokemonCustoms.clear();
            for (DocumentSnapshot documentSnapshot: collectionSnapshot) {
                counter++;
                PokemonCustom pokemonCustom = documentSnapshot.toObject(PokemonCustom.class);
                pokemonCustom.id = documentSnapshot.getId();
                pokemonCustoms.add(pokemonCustom);
                if(!pokemonCustoms.isEmpty()) Collections.sort(pokemonCustoms, new SortByFavorites().reversed());
                if (counter == 3) break;
            }
            adapter.notifyDataSetChanged();
        });


        setQuery().addSnapshotListener((collectionSnapshot2, e2) -> {
            pokemonCustoms2.clear();
            for (DocumentSnapshot documentSnapshot : collectionSnapshot2) {
                counter2++;
                PokemonCustom pokemonCustom = documentSnapshot.toObject(PokemonCustom.class);
                pokemonCustom.id = documentSnapshot.getId();
                pokemonCustoms2.add(pokemonCustom);
                if(!pokemonCustoms2.isEmpty()) Collections.sort(pokemonCustoms2, new SortByDate().reversed());
                if (counter2 == 3) break;
            }
            adapter2.notifyDataSetChanged();
        });
        binding.mostLiked.setAdapter(adapter = new MediaAdapter(getContext(), pokemonCustoms));
        binding.mostLiked.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.recentlyPublished.setAdapter(adapter2 = new MediaAdapter2(getContext(), pokemonCustoms2));
        binding.recentlyPublished.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

    }


    Query setQuery() { return db.collection("pokecustom");}

    public class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.ViewHolder> {

        private Context context;
        private ArrayList<PokemonCustom> pokemons;

        public MediaAdapter(Context context, ArrayList<PokemonCustom> pokemons) {
            this.context = context;
            this.pokemons = pokemons;
        }

        @NonNull
        @Override
        public MediaAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.media_row, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MediaAdapter.ViewHolder holder, int position) {
            PokemonCustom pokemonCustom = pokemons.get(position);
            Glide.with(context).load(pokemonCustom.sprite).into(holder.imageView);
            holder.textView.setText(pokemonCustom.name.toUpperCase());
            holder.constraintLayout.setOnClickListener(intent -> {
                Intent i = new Intent(context, CustomDetail.class);
                i.putExtra("pokemon", pokemonCustom);
                startActivity(i);
            });
        }

        @Override
        public int getItemCount() {
            return pokemons.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            ImageView imageView;
            TextView textView;
            ConstraintLayout constraintLayout;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.mediaImage);
                textView = itemView.findViewById(R.id.mediaName);
                constraintLayout = itemView.findViewById(R.id.mediarow);
            }
        }
    }

    public class MediaAdapter2 extends RecyclerView.Adapter<MediaAdapter2.ViewHolder> {

        private Context context;
        private ArrayList<PokemonCustom> pokemons;

        public MediaAdapter2(Context context, ArrayList<PokemonCustom> pokemons) {
            this.context = context;
            this.pokemons = pokemons;
        }

        @NonNull
        @Override
        public MediaAdapter2.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            View view = layoutInflater.inflate(R.layout.media_row, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MediaAdapter2.ViewHolder holder, int position) {
            PokemonCustom pokemonCustom = pokemons.get(position);
            Glide.with(context).load(pokemonCustom.sprite).into(holder.imageView);
            holder.textView.setText(pokemonCustom.name.toUpperCase());
            holder.constraintLayout.setOnClickListener(intent -> {
                Intent i = new Intent(context, CustomDetail.class);
                i.putExtra("pokemon", pokemonCustom);
                startActivity(i);
            });
        }

        @Override
        public int getItemCount() {
            return pokemons.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder{
            ImageView imageView;
            TextView textView;
            ConstraintLayout constraintLayout;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.mediaImage);
                textView = itemView.findViewById(R.id.mediaName);
                constraintLayout = itemView.findViewById(R.id.mediarow);
            }
        }
    }

    static class SortByFavorites implements Comparator<PokemonCustom> {

        @Override
        public int compare(PokemonCustom pokemonCustom1, PokemonCustom pokemonCustom2) {
            int counter1 = 1;
            int counter2 = 1;
            Set set = pokemonCustom1.favorite.entrySet();
            Iterator i = set.iterator();
            while(i.hasNext()) {
                Map.Entry me = (Map.Entry)i.next();
                if ((Boolean) me.getValue()) counter1++;
            }
            Set set2 = pokemonCustom2.favorite.entrySet();
            Iterator i2 = set2.iterator();
            while(i2.hasNext()) {
                Map.Entry me = (Map.Entry)i2.next();
                if ((Boolean) me.getValue()) counter2++;
            }
            return Integer.compare(counter1, counter2);
        }

        @Override
        public boolean equals(@Nullable Object obj) {
            return false;
        }
    }

    static class SortByDate implements Comparator<PokemonCustom> {

        @Override
        public int compare(PokemonCustom o1, PokemonCustom o2) {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            try {
                return format.parse(o1.published).compareTo(format.parse(o2.published));
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
        }

        @Override
        public boolean equals(@Nullable Object obj) {
            return false;
        }
    }

}