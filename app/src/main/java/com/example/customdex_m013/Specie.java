package com.example.customdex_m013;



import java.util.List;

public class Specie {
    int base_happiness;
    int capture_rate;
    List<Egg_group> egg_groups;
    Evolution_chain evolution_chain;
    Evolves_from_species evolves_from_species;
    List<Flavor_text_entries> flavor_text_entries;
    List<Genera> genera;
    Growth_rate growth_rate;
    Habitat habitat;
    List<Names>names;
    List<Varieties> varieties;
    int id;


    public Specie() {
    }

    public Specie(int base_happiness, int capture_rate, List<Egg_group> egg_groupList, Evolution_chain evoluntion_chain, Evolves_from_species evolves_from_species, List<Flavor_text_entries> flavor_text_entries, List<Genera> genera, Growth_rate growth_rate, Habitat habitat, Boolean has_gender_differences, List<Names> names, List<Varieties> varieties) {
        this.base_happiness = base_happiness;
        this.capture_rate = capture_rate;
        this.egg_groups = egg_groupList;
        this.evolution_chain = evoluntion_chain;
        this.evolves_from_species = evolves_from_species;
        this.flavor_text_entries = flavor_text_entries;
        this.genera = genera;
        this.growth_rate = growth_rate;
        this.habitat = habitat;
        this.names = names;
        this.varieties = varieties;
    }
}
class Egg_group{
    String name;
    String url;
}
class Evolution_chain{
    String url;
}
class Evolves_from_species{
    String name;
    String url;

}
class Flavor_text_entries{
    String flavor_text;
     Language language;
     Version version;
}
class Language{
    String name;
    String url;
}
class Genera{
    String genus;
    Language language;
}
class Growth_rate{
    String name;
    String url;
}
class Habitat {
    String name;
    String url;
}
class Names{
    Language language;
    String name;
}
class Varieties{
    boolean is_default;
    PoketMonster pokemon;
}
class PoketMonster{
    String name;
    String url;
}
