package com.example.customdex_m013;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.customdex_m013.costumModel.AbilitiesCostum;
import com.example.customdex_m013.costumModel.MovesCostum;
import com.example.customdex_m013.costumModel.StatsCostum;
import com.example.customdex_m013.databinding.FragmentAddPokemonBinding;
import com.example.customdex_m013.model.PokemonCustom;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.google.gson.TypeAdapter;
import com.squareup.picasso.Picasso;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.UUID;


public class AddPokemonFragment extends Fragment {

    private ArrayList<TypeSpinner> types;
    private ArrayList<PokemonCustom> pokemonCustoms = new ArrayList<>();
    private TypeAdapter typeAdapter;
    private FragmentAddPokemonBinding binding;
    private AppViewModel appViewModel;
    private ArrayList<StatsCostum> statsCostums = new ArrayList<>();
    private ArrayList<AbilitiesCostum> abilitiesCostums = new ArrayList<>();
    private ArrayList<MovesCostum> movesCostums = new ArrayList<>();
    private Uri uriImagen;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private PokemonCustom pokemonSelected;

    String moves = "";
    String levels = "";
    String tipo1 = "";
    String tipo2 = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return (binding = FragmentAddPokemonBinding.inflate(inflater, container, false)).getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        appViewModel = new ViewModelProvider(requireActivity()).get(AppViewModel.class);

        getTypes();

        typeAdapter = new TypeAdapter(getContext(), types);
        binding.type1.setAdapter(typeAdapter);
        binding.type2.setAdapter(typeAdapter);


        binding.selectBasePoke.setOnClickListener( v -> {
            startActivityForResult(new Intent(requireActivity(), SelectPokemonActivity.class), 1234);
        });


        binding.type1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TypeSpinner selected = (TypeSpinner) adapterView.getItemAtPosition(i);
                if (adapterView.getSelectedItemPosition() == binding.type2.getSelectedItemPosition()) {
                    Toast.makeText(getContext(), "No se pueden repetir los tipos", Toast.LENGTH_SHORT).show();
                    binding.type2.setSelection(18);
                }

                if (selected.getImage() == R.drawable.desconocido) {
                    Toast.makeText(getContext(), "No puede ser nulo", Toast.LENGTH_SHORT).show();
                    adapterView.setSelection(0);
                }
                if (selected.getImage() == R.drawable.acero) tipo1 = "Acero";

                if (selected.getImage() == R.drawable.agua) tipo1 = "Agua";

                if (selected.getImage() == R.drawable.bicho) tipo1 = "Bicho";

                if (selected.getImage() == R.drawable.dragon) tipo1 = "Dragon";

                if (selected.getImage() == R.drawable.electrico) tipo1 = "Electrico";

                if (selected.getImage() == R.drawable.fantasma) tipo1 = "Fantasma";

                if (selected.getImage() == R.drawable.fuego) tipo1 = "Fuego";

                if (selected.getImage() == R.drawable.hada) tipo1 = "Hada";

                if (selected.getImage() == R.drawable.hielo) tipo1 = "Hielo";

                if (selected.getImage() == R.drawable.lucha) tipo1 = "Lucha";

                if (selected.getImage() == R.drawable.normal) tipo1 = "Normal";

                if (selected.getImage() == R.drawable.planta) tipo1 = "Planta";

                if (selected.getImage() == R.drawable.psiquico) tipo1 = "Psíquico";

                if (selected.getImage() == R.drawable.roca) tipo1 = "Roca";

                if (selected.getImage() == R.drawable.siniestro) tipo1 = "Siniestro";

                if (selected.getImage() == R.drawable.tierra) tipo1 = "Tierra";

                if (selected.getImage() == R.drawable.veneno) tipo1 = "Veneno";

                if (selected.getImage() == R.drawable.volador) tipo1 = "Volador";

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(getContext(), "No puede ser nulo", Toast.LENGTH_SHORT).show();
            }
        });

        binding.type2.setSelection(18);
        binding.type2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                TypeSpinner selected = (TypeSpinner) adapterView.getItemAtPosition(i);
                if (adapterView.getSelectedItemPosition() == binding.type1.getSelectedItemPosition()) {
                    Toast.makeText(getContext(), "No se pueden repetir los tipos", Toast.LENGTH_SHORT).show();
                    adapterView.setSelection(18);
                }

                if (selected.getImage() == R.drawable.acero) tipo2 = "Acero";

                if (selected.getImage() == R.drawable.agua) tipo2 = "Agua";

                if (selected.getImage() == R.drawable.bicho) tipo2 = "Bicho";

                if (selected.getImage() == R.drawable.dragon) tipo2 = "Dragon";

                if (selected.getImage() == R.drawable.electrico) tipo2 = "Electrico";

                if (selected.getImage() == R.drawable.fantasma) tipo2 = "Fantasma";

                if (selected.getImage() == R.drawable.fuego) tipo2 = "Fuego";

                if (selected.getImage() == R.drawable.hada) tipo2 = "Hada";

                if (selected.getImage() == R.drawable.hielo) tipo2 = "Hielo";

                if (selected.getImage() == R.drawable.lucha) tipo2 = "Lucha";

                if (selected.getImage() == R.drawable.normal) tipo2 = "Normal";

                if (selected.getImage() == R.drawable.planta) tipo2 = "Planta";

                if (selected.getImage() == R.drawable.psiquico) tipo2 = "Psíquico";

                if (selected.getImage() == R.drawable.roca) tipo2 = "Roca";

                if (selected.getImage() == R.drawable.siniestro) tipo2 = "Siniestro";

                if (selected.getImage() == R.drawable.tierra) tipo2 = "Tierra";

                if (selected.getImage() == R.drawable.veneno) tipo2 = "Veneno";

                if (selected.getImage() == R.drawable.volador) tipo2 = "Volador";

                if (selected.getImage() == R.drawable.desconocido) tipo2 = "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        binding.addMove.setOnClickListener(add -> {

            if (!binding.writtenMove.getText().toString().equals("") && !binding.writtenLevel.getText().toString().equals("")) {
                movesCostums.add(new MovesCostum(binding.writtenMove.getText().toString(), binding.writtenLevel.getText().toString()));

                moves += binding.writtenMove.getText().toString() + "\n";
                binding.addedMoves.setText(moves);
                binding.writtenMove.setText("");

                levels += binding.writtenLevel.getText().toString() + "\n";
                binding.addedLevels.setText(levels);
                binding.writtenLevel.setText("");

            } else {
                Toast.makeText(getContext(), "Debes introducir los dos campos", Toast.LENGTH_SHORT).show();
            }
        });
        binding.addSprite.setImageResource(R.drawable.pokeball);
        binding.addSprite.setOnClickListener(addSprite -> {
            galeria.launch("image/*");
        });

        appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri-> {
            if(uri != null) {
                Glide.with(this).load(uri).into(binding.addSprite);
                uriImagen = uri;
            }
        });

        binding.sendCustomPoke.setOnClickListener(send-> {
            if(binding.customHeight.getText().toString().equals("") || binding.customWeight.getText().toString().equals("") ||
                    binding.customName.getText().toString().equals("") || binding.customAbility1.getText().toString().equals("") ||
            binding.customAbility0.getText().equals("") || binding.customDescription.getText().toString().equals("") ||
                    binding.customAttack.getText().toString().equals("") || binding.customSpecAttack.getText().toString().equals("") ||
            binding.customDefense.getText().toString().equals("") || binding.customSpecDef.getText().toString().equals("") ||
            binding.customVelocity.getText().toString().equals("") || binding.customHP.getText().toString().equals("") ||
                    binding.customClasse.getText().toString().equals("")) {
                Toast.makeText(getContext(), "Faltan campos obligatorios por rellenar", Toast.LENGTH_SHORT).show();
            } else {
                statsCostums.add(new StatsCostum(binding.customAttack.getText().toString(), binding.customDefense.getText().toString(),
                        binding.customSpecDef.getText().toString(), binding.customSpecAttack.getText().toString(), binding.customVelocity.getText().toString(),
                        binding.customHP.getText().toString()));
                abilitiesCostums.add(new AbilitiesCostum(binding.customAbility1.getText().toString(), binding.customAbility2.getText().toString(), binding.customAbility0.getText().toString()));
                movesCostums.add(new MovesCostum(binding.addedMoves.getText().toString(), binding.addedLevels.getText().toString()));

                binding.sendCustomPoke.setEnabled(false);



                FirebaseStorage.getInstance()
                        .getReference("/pokecustom/"+ UUID.randomUUID())
                        .putFile(uriImagen)
                        .continueWithTask(task -> task.getResult().getStorage().getDownloadUrl())
                        .addOnSuccessListener(urlDescarga -> {
                            PokemonCustom pokemonCustom = new PokemonCustom();
                            pokemonCustom.id = UUID.randomUUID().toString();
                            pokemonCustom.name = binding.customName.getText().toString();
                            pokemonCustom.classe = binding.customClasse.getText().toString();
                            pokemonCustom.description = binding.customDescription.getText().toString();
                            pokemonCustom.height = binding.customHeight.getText().toString();
                            pokemonCustom.weight = binding.customWeight.getText().toString();
                            pokemonCustom.type1 = tipo1;
                            pokemonCustom.type2 = tipo2;
                            pokemonCustom.abilities = abilitiesCostums;
                            pokemonCustom.moves = movesCostums;
                            pokemonCustom.stats = statsCostums;
                            pokemonCustom.sprite = urlDescarga.toString();
                            pokemonCustom.authorName = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
                            pokemonCustom.published = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
                            if (pokemonSelected == null) pokemonCustom.preEvolution = null;
                            else pokemonCustom.preEvolution = pokemonSelected.name;



                            FirebaseFirestore.getInstance().collection("pokecustom")
                                    .add(pokemonCustom)
                                    .addOnCompleteListener(task -> {
                                        binding.sendCustomPoke.setEnabled(true);
                                        binding.customName.setText("");
                                        binding.customClasse.setText("");
                                        binding.customDescription.setText("");
                                        binding.customWeight.setText("");
                                        binding.customHeight.setText("");
                                        binding.addSprite.setImageResource(R.drawable.pokeball);
                                        binding.type1.setSelection(0);
                                        binding.type2.setSelection(18);
                                        binding.customAbility1.setText("");
                                        binding.customAbility2.setText("");
                                        binding.customAbility0.setText("");
                                        binding.customAttack.setText("");
                                        binding.customSpecAttack.setText("");
                                        binding.customDefense.setText("");
                                        binding.customSpecDef.setText("");
                                        binding.customVelocity.setText("");
                                        binding.customHP.setText("");
                                        binding.addedMoves.setText("");
                                        binding.addedLevels.setText("");
                                        statsCostums.clear();
                                        movesCostums.clear();
                                        abilitiesCostums.clear();
                                    });
                        });
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
            pokemonSelected = (PokemonCustom) data.getSerializableExtra("pokemonSelected");

    }

    class TypeAdapter extends ArrayAdapter<TypeSpinner> {

        public TypeAdapter(@NonNull Context context, ArrayList<TypeSpinner> typeSpinnerArrayList) {
            super(context, 0, typeSpinnerArrayList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        @Override
        public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            return initView(position, convertView, parent);
        }

        private View initView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.typerow, parent, false);
            }
            ImageView imageView = convertView.findViewById(R.id.typeImage);
            TypeSpinner currentItem = getItem(position);
            if (currentItem != null) {
                imageView.setImageResource(currentItem.getImage());
            }
            return convertView;
        }
    }

    public void getTypes() {
        types = new ArrayList<>();
        types.add(new TypeSpinner(R.drawable.acero));
        types.add(new TypeSpinner(R.drawable.agua));
        types.add(new TypeSpinner(R.drawable.bicho));
        types.add(new TypeSpinner(R.drawable.dragon));
        types.add(new TypeSpinner(R.drawable.electrico));
        types.add(new TypeSpinner(R.drawable.fantasma));
        types.add(new TypeSpinner(R.drawable.fuego));
        types.add(new TypeSpinner(R.drawable.hada));
        types.add(new TypeSpinner(R.drawable.hielo));
        types.add(new TypeSpinner(R.drawable.lucha));
        types.add(new TypeSpinner(R.drawable.normal));
        types.add(new TypeSpinner(R.drawable.planta));
        types.add(new TypeSpinner(R.drawable.psiquico));
        types.add(new TypeSpinner(R.drawable.roca));
        types.add(new TypeSpinner(R.drawable.siniestro));
        types.add(new TypeSpinner(R.drawable.tierra));
        types.add(new TypeSpinner(R.drawable.veneno));
        types.add(new TypeSpinner(R.drawable.volador));
        types.add(new TypeSpinner(R.drawable.desconocido));

    }

    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        appViewModel.setUriImagenSeleccionada(uri);
    });


}