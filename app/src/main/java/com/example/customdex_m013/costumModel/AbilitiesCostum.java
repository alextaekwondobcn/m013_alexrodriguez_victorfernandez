package com.example.customdex_m013.costumModel;

import java.io.Serializable;

public class AbilitiesCostum implements Serializable {
    public String ability1;
    public String ability2;
    public String ability0;

    public AbilitiesCostum(String ability1, String ability2, String ability0) {
        this.ability1 = ability1;
        this.ability2 = ability2;
        this.ability0 = ability0;
    }

    public AbilitiesCostum() {
    }
}
