package com.example.customdex_m013.costumModel;

import java.io.Serializable;

public class StatsCostum implements Serializable {
    public String attack;
    public String defense;
    public String specialDefense;
    public String specialAttack;
    public String velocity;
    public String healthPoints;

    public StatsCostum(String attack, String defense, String specialDefense, String specialAttack, String velocity, String healthPoints) {
        this.attack = attack;
        this.defense = defense;
        this.specialDefense = specialDefense;
        this.specialAttack = specialAttack;
        this.velocity = velocity;
        this.healthPoints = healthPoints;
    }

    public StatsCostum() {
    }
}
