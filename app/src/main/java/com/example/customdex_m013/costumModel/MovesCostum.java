package com.example.customdex_m013.costumModel;

import java.io.Serializable;

public class MovesCostum implements Serializable {
    public String moveName;
    public String moveLvl;
    public String id;

    private static int idNext = 1;

    public MovesCostum(String moveName, String moveLvl) {
        this.moveName = moveName;
        this.moveLvl = moveLvl;
        String str = String.format("%07d", id);
        this.id = "MOVE" + str;

        idNext++;
    }

    public MovesCostum() {
    }


}
