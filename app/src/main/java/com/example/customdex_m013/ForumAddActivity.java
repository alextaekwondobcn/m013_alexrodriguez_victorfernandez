package com.example.customdex_m013;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;

import android.os.Bundle;
import android.widget.Button;

import com.example.customdex_m013.model.Forum;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class ForumAddActivity extends AppCompatActivity {

    private Button publicar;
    private TextInputEditText premisa;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_add);

        publicar = findViewById(R.id.publicarForo);
        premisa = findViewById(R.id.premisaInput);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        publicar.setOnClickListener(publish -> {
            publicar.setEnabled(false);
            Forum forum = new Forum();
            forum.premise = premisa.getText().toString();
            forum.url = String.valueOf(FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl());
            forum.forumId = UUID.randomUUID().toString();
            forum.authorName = String.valueOf(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
            forum.date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));

            FirebaseFirestore.getInstance().collection("forums")
                    .add(forum)
                    .addOnCompleteListener(task -> {
                        publicar.setEnabled(true);
                        finish();
                    });
        });

    }
}