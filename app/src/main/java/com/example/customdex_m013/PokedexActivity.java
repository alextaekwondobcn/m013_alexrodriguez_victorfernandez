package com.example.customdex_m013;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

public class PokedexActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
     String pokeapi="https://pokeapi.co/api/v2/pokemon/";
    private ArrayList<Pokemon> pokemones = new ArrayList<>();
    private MyAdapter myAdapter;
    String nameGen;
    Toolbar toolbar;
    ProgressBar progressBar;
    SlotType type1;
    SlotType type2;
    int slotPos1;
    int slotPos2;
    String typeName1;
    String typeName2;
    String busqueda;
     int startPokemon = 1;
     int endPokemon = 20;
     int contador = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokedex);
        recyclerView = findViewById(R.id.recyclerPoke);
        nameGen = getIntent().getStringExtra("name");
        progressBar = findViewById(R.id.prog1);
        busqueda = getIntent().getStringExtra("searched");

        if(busqueda==null){
            getPokemons();
        }else{
            getSearchedPokemon();
        }
        //getPokemons();
        toolbar = (Toolbar) findViewById(R.id.toolbar);


        setSupportActionBar(toolbar);
        getSupportActionBar().setLogo(R.drawable.customdexlogo);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

    }
    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
        private final ArrayList<Pokemon> pokemons;
        private final Context context;

        public MyAdapter(ArrayList<Pokemon> pokemons, Context context) {
            this.pokemons = pokemons;
            this.context = context;
        }
        @NonNull
        @Override
        public MyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.pokemon_row, parent,false));
        }

        @Override
        public void onBindViewHolder(@NonNull MyAdapter.MyViewHolder holder, int position) {
            Pokemon pokemon = pokemons.get(holder.getAdapterPosition());

            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(holder.imageView);
            holder.textName.setText(pokemon.name.toUpperCase(Locale.ROOT));

            holder.rowPoke.setOnClickListener(view -> {
                Intent i = new Intent(context, PokemonActivity.class);
                i.putExtra("name", pokemons.get(holder.getAdapterPosition()).name);
                i.putExtra("sprite", pokemons.get(holder.getAdapterPosition()).sprites.front_default);
                i.putExtra("specie", pokemons.get(holder.getAdapterPosition()).species.url);
                i.putExtra("altura", pokemons.get(holder.getAdapterPosition()).height);
                i.putExtra("peso", pokemons.get(holder.getAdapterPosition()).weight);
                i.putExtra("spriteBack", pokemons.get(holder.getAdapterPosition()).sprites.back_default);
                i.putExtra("spriteShiny", pokemons.get(holder.getAdapterPosition()).sprites.front_shiny);
                i.putExtra("backShiny", pokemons.get(holder.getAdapterPosition()).sprites.back_shiny);
                i.putExtra("id", pokemons.get(holder.getAdapterPosition()).id);
                i.putExtra("frontFemale", pokemons.get(holder.getAdapterPosition()).sprites.front_female);
                i.putExtra("backFemale", pokemons.get(holder.getAdapterPosition()).sprites.back_female);
                i.putExtra("shinyFemale", pokemons.get(holder.getAdapterPosition()).sprites.front_shiny_female);
                i.putExtra("backShinyFemale", pokemons.get(holder.getAdapterPosition()).sprites.back_shiny_female);

                for(Abilities abilities:pokemons.get(holder.getAdapterPosition()).abilities){
                    if(abilities.is_hidden!=true&&abilities.slot==1){
                        i.putExtra("habilidad1", abilities.ability.name);
                    }else if(abilities.is_hidden!=true&&abilities.slot==2){
                        i.putExtra("habilidad2", abilities.ability.name);
                    }else if(abilities.is_hidden){
                        i.putExtra("habilidadoculta",abilities.ability.name);
                    }
                }
                for(Stats stats : pokemons.get(holder.getAdapterPosition()).stats){
                    if(stats.stat.name.equals("hp")){
                        i.putExtra("hp",stats.base_stat);
                    }else if(stats.stat.name.equals("attack")){
                        i.putExtra("atk", stats.base_stat);
                    }else if(stats.stat.name.equals("defense")){
                        i.putExtra("def",stats.base_stat);
                    }else if(stats.stat.name.equals("special-attack")){
                        i.putExtra("spAtk",stats.base_stat);
                    }else if(stats.stat.name.equals("special-defense")){
                        i.putExtra("spDef",stats.base_stat);
                    }else if(stats.stat.name.equals("speed")){
                        i.putExtra("speed", stats.base_stat);
                    }
                }


                int s=0;
                for(SlotType slotType: pokemons.get(holder.getAdapterPosition()).types){
                    i.putExtra("slot_num"+s, slotType.slot);
                    i.putExtra("type_name"+s, slotType.type.name);
                    s++;
                }

                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            });
        }

        @Override
        public int getItemCount() {
//            System.out.println("LISTA SIZE " + pokemons.size());
            return pokemons.size();
        }

        public  class MyViewHolder extends RecyclerView.ViewHolder {
            private final ImageView imageView;
            private final TextView textName;
            private ConstraintLayout rowPoke;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.poksprite);
                textName = itemView.findViewById(R.id.pokename);
                rowPoke = itemView.findViewById(R.id.thepokerow);

            }
        }
    }

    private void getSearchedPokemon(){
        Volley.newRequestQueue(this).add(new JsonObjectRequest(
                Request.Method.GET,
                pokeapi + busqueda+"/",
                null,
                response -> {
                    JSONObject pokeObject = response;
                    Pokemon pok1 = new GsonBuilder().create().fromJson(pokeObject.toString(), Pokemon.class);
                    pokemones.add(pok1);
                    myAdapter = new MyAdapter(pokemones, getApplicationContext());
                    progressBar.setVisibility(View.INVISIBLE);
                    progressBar.setActivated(false);
                    recyclerView.setAdapter(myAdapter);
                    /*if (contador == endPokemon- startPokemon + 1) {
                        pokemones.sort(Comparator.comparingInt(pokemon -> pokemon.id));
                        pokemones.forEach(p -> System.out.println(p.id + " :: " + p.name));
                        myAdapter = new MyAdapter(pokemones, getApplicationContext());
                        progressBar.setVisibility(View.INVISIBLE);
                        progressBar.setActivated(false);
                        recyclerView.setAdapter(myAdapter);
                    }*/
                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
        ));
    }

    private void getPokemons(){

        if(nameGen.equals("Kanto")){
            startPokemon = 1;
            endPokemon = 151;
        }else if(nameGen.equals("Johto")){
            startPokemon =152;
            endPokemon=251;
        }else if(nameGen.equals("Hoenn")){
            startPokemon =252;
            endPokemon=386;
        }else if(nameGen.equals("Sinnoh")){
            startPokemon =387;
            endPokemon=493;
        }else if(nameGen.equals("Teselia")){
            startPokemon =494;
            endPokemon=649;
        }else if(nameGen.equals("Kalos")){
            startPokemon =650;
            endPokemon=721;
        }else if(nameGen.equals("Alola")){
            startPokemon =722;
            endPokemon=809;
        }else if(nameGen.equals("Galar")){
            startPokemon =810;
            endPokemon=898;
        } else {
            Intent i = new Intent(this, CustomdexActivity.class);
            startActivity(i);
        }

        //endPokemon = startPokemon + 10;

        contador = 0;
        // CADA VEZ QUE HAYA UN "RESPONSE" AUMENTAR EL CONTADOR, CUANDO EL CONTADOR SEA IGUAL AL TOTAL, ENTONCES ORDENAR Y MOSTRAR
        for (int i = startPokemon; i <= endPokemon; i++) {
            int ii = i;
            Volley.newRequestQueue(this).add(new JsonObjectRequest(
                    Request.Method.GET,
                    pokeapi + i + "/",
                    null,
                    response -> {
                        contador++;
                        System.out.println("OBTENIDO POKEMON " + ii);
                        JSONObject pokeObject = response;
                        Pokemon pok1 = new GsonBuilder().create().fromJson(pokeObject.toString(), Pokemon.class);
                        pokemones.add(pok1);

                        if (contador == endPokemon- startPokemon + 1) {
                            pokemones.sort(Comparator.comparingInt(pokemon -> pokemon.id));
                            pokemones.forEach(p -> System.out.println(p.id + " :: " + p.name));
                            myAdapter = new MyAdapter(pokemones, getApplicationContext());
                            progressBar.setVisibility(View.INVISIBLE);
                            progressBar.setActivated(false);
                            recyclerView.setAdapter(myAdapter);
                        }
                    },
                    error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
            ));
        }
    }
}

/*
1-------------------------|
2-------------------------|
4--------
5-------------------------|


 */
