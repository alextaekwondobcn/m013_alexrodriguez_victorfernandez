package com.example.customdex_m013;

public class Generation {
    public String name;
    public int url;
    public int urlName;

    public Generation() {
        }

        public Generation(String name, int url, int urlName) {
            this.name = name;
            this.url = url;
            this.urlName = urlName;
        }

}
