package com.example.customdex_m013;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.customdex_m013.model.PokemonCustom;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;


public class FavoriteFragment extends Fragment {
    private ArrayList<PokemonCustom> profilepoke = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    public FirebaseAuth auth = FirebaseAuth.getInstance();
    public FirebaseStorage storage = FirebaseStorage.getInstance();
    public AppViewModel appViewModel;
    public FavAdapter myAdapter;
    private RecyclerView recyclerView;


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        appViewModel = new ViewModelProvider(requireActivity()).get(AppViewModel.class);

        recyclerView = view.findViewById(R.id.favrecycler);

        setQuery().addSnapshotListener((collectionSnapshot, e)->{
            profilepoke.clear();
            for(DocumentSnapshot snap :collectionSnapshot){
                PokemonCustom pok = snap.toObject(PokemonCustom.class);
                pok.id = snap.getId();
                profilepoke.add(pok);
            }
            myAdapter.notifyDataSetChanged();
        });

        myAdapter = new FavAdapter(profilepoke, getContext());
        recyclerView.setAdapter(myAdapter);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorite, container, false);
    }
    Query setQuery() { return db.collection("pokecustom").whereEqualTo("favorite."+auth.getUid(),true); }

    public class FavAdapter extends RecyclerView.Adapter<FavoriteFragment.FavAdapter.MyViewHolder>{
        private final ArrayList<PokemonCustom> pokemons;
        private final Context context;

        public FavAdapter(ArrayList<PokemonCustom> pokemons, Context context) {
            this.pokemons = pokemons;
            this.context = context;
        }

        @NonNull
        @Override
        public FavoriteFragment.FavAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new FavoriteFragment.FavAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.pokemon_row, parent, false));

        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            PokemonCustom pokemon = pokemons.get(holder.getAdapterPosition());
            Picasso.get().load(pokemon.sprite).fit().centerCrop().into(holder.imageView);
            holder.textName.setText(pokemon.name.toUpperCase(Locale.ROOT));

            holder.lay.setOnClickListener(detail -> {
                Intent i = new Intent(context, CustomDetail.class);
                i.putExtra("pokemon", pokemon);
                context.startActivity(i);
                    });

        }



        @Override
        public int getItemCount() {
            return pokemons.size();

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private final ImageView imageView;
            private final TextView textName;
            private final ConstraintLayout lay;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.poksprite);
                textName = itemView.findViewById(R.id.pokename);
                lay = itemView.findViewById(R.id.thepokerow);
            }
        }
    }
}