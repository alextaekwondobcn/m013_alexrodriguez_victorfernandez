package com.example.customdex_m013;

public class TypeSpinner {
    public int image;

    public TypeSpinner(int image) {
        this.image = image;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
