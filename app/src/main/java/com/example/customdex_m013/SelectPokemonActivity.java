package com.example.customdex_m013;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;

import com.example.customdex_m013.databinding.ActivityForumBinding;
import com.example.customdex_m013.databinding.ActivitySelectPokemonBinding;
import com.example.customdex_m013.databinding.PokecustomRowBinding;
import com.example.customdex_m013.model.PokemonCustom;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SelectPokemonActivity extends AppCompatActivity {

    private ActivitySelectPokemonBinding binding;
    private ArrayList<PokemonCustom> pokemonCustoms = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private PokecostumAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivitySelectPokemonBinding.inflate(getLayoutInflater())).getRoot());

        setQuery().addSnapshotListener((collectionSnapshot, e) -> {
            pokemonCustoms.clear();
            for (DocumentSnapshot documentSnapshot: collectionSnapshot) {
                PokemonCustom pokemonCustom = documentSnapshot.toObject(PokemonCustom.class);
                pokemonCustom.id = documentSnapshot.getId();
                pokemonCustoms.add(pokemonCustom);
            }
            adapter.notifyDataSetChanged();
        });
        binding.pokemonsCustomRecycler.setAdapter(adapter = new PokecostumAdapter(this));
        binding.pokemonsCustomRecycler.setLayoutManager(new LinearLayoutManager(this));
    }

    Query setQuery() { return db.collection("pokecustom").whereEqualTo("authorName", FirebaseAuth.getInstance().getCurrentUser().getDisplayName()); }

    class PokecostumAdapter extends RecyclerView.Adapter<PokecostumAdapter.ViewHolder> {

        private final Context context;

        public PokecostumAdapter(Context context) {
            this.context = context;
        }

        @NonNull
        @Override
        public PokecostumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new PokecostumAdapter.ViewHolder(PokecustomRowBinding.inflate(getLayoutInflater(), parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull PokecostumAdapter.ViewHolder holder, int position) {
            PokemonCustom pokemonCustom = pokemonCustoms.get(position);
            holder.binding.customNamerow.setText(pokemonCustom.name);
            Picasso.get().load(pokemonCustom.sprite).fit().centerCrop().into(holder.binding.customSprite);
            holder.binding.customrow.setOnClickListener(detail -> {
                Intent intent = new Intent();
                intent.putExtra("pokemonSelected", pokemonCustom);
                setResult(1234, intent);
                finish();
            });
        }

        @Override
        public int getItemCount() {
            return pokemonCustoms.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            PokecustomRowBinding binding;

            public ViewHolder(PokecustomRowBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
}