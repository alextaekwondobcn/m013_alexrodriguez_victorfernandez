package com.example.customdex_m013;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.example.customdex_m013.databinding.ActivityMainBinding;
import com.example.customdex_m013.databinding.ActivitySignInBinding;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;

public class SignInActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private ActivitySignInBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivitySignInBinding.inflate(getLayoutInflater())).getRoot());
        firebaseAuth = FirebaseAuth.getInstance();


        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build());

        firebaseAuthWithGoogle(GoogleSignIn.getLastSignedInAccount(this));

        binding.signinbutton.setOnClickListener(googleSignIn -> {
            signInClient.launch(googleSignInClient.getSignInIntent());
        });
    }


    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        System.out.println("entra 878778787887");
        if(account == null) return;
        System.out.println("entra 999999");
        FirebaseAuth.getInstance().signInWithCredential(GoogleAuthProvider.getCredential(account.getIdToken(), null))
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        System.out.println("entra");
                        startActivity(new Intent(this, MainActivity.class));
                    }else{
                        System.out.println("no entra");
                    }
                });
    }

    ActivityResultLauncher<Intent> signInClient = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    try {
                        System.out.println("jajajaj 11111111");
                        firebaseAuthWithGoogle(GoogleSignIn.getSignedInAccountFromIntent(result.getData()).getResult(ApiException.class));
                    } catch (ApiException e) {
                        System.out.println("jajajaj 45454545");
                    }
                } else {
                    System.out.println("not okokok " + result.getResultCode());
                }
            });

}