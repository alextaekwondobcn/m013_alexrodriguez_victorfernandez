package com.example.customdex_m013;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.example.customdex_m013.databinding.ActivityCustomdexBinding;
import com.example.customdex_m013.databinding.ActivityForumBinding;
import com.example.customdex_m013.databinding.ForumRowBinding;
import com.example.customdex_m013.databinding.PokecustomRowBinding;
import com.example.customdex_m013.databinding.PokemonRowBinding;
import com.example.customdex_m013.model.Forum;
import com.example.customdex_m013.model.PokemonCustom;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CustomdexActivity extends AppCompatActivity {

    private ArrayList<PokemonCustom> pokemonCustoms = new ArrayList<>();
    private PokecostumAdapter adapter;
    private ActivityCustomdexBinding binding;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivityCustomdexBinding.inflate(getLayoutInflater())).getRoot());

        binding.pokemonsCustom.setAdapter(adapter = new PokecostumAdapter(this));
        binding.pokemonsCustom.setLayoutManager(new LinearLayoutManager(this));

        setQuery().addSnapshotListener((collectionSnapshot, e) -> {
            pokemonCustoms.clear();
            for (DocumentSnapshot documentSnapshot: collectionSnapshot) {
                PokemonCustom pokemonCustom = documentSnapshot.toObject(PokemonCustom.class);
                pokemonCustom.id = documentSnapshot.getId();
                pokemonCustoms.add(pokemonCustom);
                Collections.sort(pokemonCustoms, new SortById());
            }
            adapter.notifyDataSetChanged();
        });

    }

    Query setQuery() { return db.collection("pokecustom"); }

    class PokecostumAdapter extends RecyclerView.Adapter<PokecostumAdapter.ViewHolder> {

        private final Context context;

        public PokecostumAdapter(Context context) {
            this.context = context;
        }

        @NonNull
        @Override
        public PokecostumAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ViewHolder(PokecustomRowBinding.inflate(getLayoutInflater(), parent, false));        }

        @Override
        public void onBindViewHolder(@NonNull PokecostumAdapter.ViewHolder holder, int position) {
            PokemonCustom pokemonCustom = pokemonCustoms.get(position);
            holder.binding.customNamerow.setText(pokemonCustom.name);
            Picasso.get().load(pokemonCustom.sprite).fit().centerCrop().into(holder.binding.customSprite);
            holder.binding.customrow.setOnClickListener(detail -> {
                Intent i = new Intent(getApplicationContext(), CustomDetail.class);
                i.putExtra("name", pokemonCustom.name);
                i.putExtra("clase", pokemonCustom.classe);
                i.putExtra("description", pokemonCustom.description);
                i.putExtra("weight", pokemonCustom.weight);
                i.putExtra("height", pokemonCustom.height);
                i.putExtra("type1", pokemonCustom.type1);
                i.putExtra("type2", pokemonCustom.type2);
                i.putExtra("sprite", pokemonCustom.sprite);
                i.putExtra("moves", pokemonCustom.moves);
                i.putExtra("stats", pokemonCustom.stats);
                i.putExtra("abilities", pokemonCustom.abilities);
                i.putExtra("authorName", pokemonCustom.authorName);
                i.putExtra("id", pokemonCustom.id);
                i.putExtra("pok", pokemonCustom);
                i.putExtra("preEvolution", pokemonCustom.preEvolution);
                context.startActivity(i);
            });
        }

        @Override
        public int getItemCount() {
            return pokemonCustoms.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            PokecustomRowBinding binding;
            public ViewHolder(PokecustomRowBinding binding) {
                super(binding.getRoot());
                this.binding = binding;
            }
        }
    }
    static class SortById implements Comparator<PokemonCustom> {

        @Override
        public int compare(PokemonCustom poke1, PokemonCustom poke2) {
            return poke1.id.compareTo(poke2.id);
        }

        @Override
        public boolean equals(Object o) {
            return false;
        }
    }
}