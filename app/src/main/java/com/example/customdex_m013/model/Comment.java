package com.example.customdex_m013.model;

public class Comment {
    public String content;
    public boolean like;
    public String url;
    public String authorName;
    public String commentId;
    public String forumId;
    public String date;

    public Comment(String content, boolean like, String url, String authorName, String commentId, String forumId, String date) {
        this.content = content;
        this.like = like;
        this.url = url;
        this.authorName = authorName;
        this.commentId = commentId;
        this.forumId = forumId;
        this.date = date;
    }

    public Comment() {
    }
}
