package com.example.customdex_m013.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Forum {
    public String forumId;
    public String premise;
    public String url;
    public String authorName;
    public String date;

    public Forum(String forumId, String premise, String url, String authorName, String date) {
        this.forumId = forumId;
        this.premise = premise;
        this.url = url;
        this.authorName = authorName;
        this.date = date;
    }

    public Forum() {
    }

}
