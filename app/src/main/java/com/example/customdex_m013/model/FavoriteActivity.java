package com.example.customdex_m013.model;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.widget.FrameLayout;

import com.example.customdex_m013.FavoriteFragment;
import com.example.customdex_m013.R;

public class FavoriteActivity extends AppCompatActivity {
    private FrameLayout frameLayout;
    private FavoriteFragment favoriteFragment;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        frameLayout = findViewById(R.id.faveFrame);
        favoriteFragment = new FavoriteFragment();
        setFragment(favoriteFragment);

    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.faveFrame, fragment);
        fragmentTransaction.commit();
    }
}