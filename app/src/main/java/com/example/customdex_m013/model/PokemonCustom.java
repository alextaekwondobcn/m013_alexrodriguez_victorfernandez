package com.example.customdex_m013.model;

import com.example.customdex_m013.Pokemon;
import com.example.customdex_m013.costumModel.AbilitiesCostum;
import com.example.customdex_m013.costumModel.MovesCostum;
import com.example.customdex_m013.costumModel.StatsCostum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class PokemonCustom implements Serializable {
    public String authorName;
    public String id;
    public String name;
    public String classe;
    public String description;
    public String weight;
    public String height;
    public String type1;
    public String type2;
    public String sprite;
    public String published;
    public ArrayList<MovesCostum> moves;
    public ArrayList<AbilitiesCostum> abilities;
    public ArrayList<StatsCostum> stats;
    public HashMap<String, Boolean> favorite = new HashMap<>();
    public String preEvolution;

    public PokemonCustom(String authorName, String id, String name, String classe, String description, String weight, String height,
                         String type1, String type2, String sprite, String published, ArrayList<MovesCostum> moves,
                         ArrayList<AbilitiesCostum> abilities, ArrayList<StatsCostum> stats, String preEvolution) {
        this.authorName = authorName;
        this.id = id;
        this.name = name;
        this.description = description;
        this.weight = weight;
        this.height = height;
        this.type1 = type1;
        this.type2 = type2;
        this.sprite = sprite;
        this.published = published;
        this.moves = moves;
        this.abilities = abilities;
        this.stats = stats;
        this.classe = classe;
        this.preEvolution = preEvolution;
    }

    public void setFavorite(HashMap<String, Boolean> favorite) {
        this.favorite = favorite;
    }

    public PokemonCustom() {
        favorite = new HashMap<>();
    }
}
