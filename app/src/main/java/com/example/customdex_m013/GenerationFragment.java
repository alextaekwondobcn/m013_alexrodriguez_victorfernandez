package com.example.customdex_m013;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Locale;


public class GenerationFragment extends Fragment {
    ArrayList<Generation> generations = new ArrayList<>();
    RecyclerView recyclerView;
    GenAdapter genAdapter;
    ImageView searcher;
    TextInputEditText inserter;
    TextInputLayout layer;
    ImageView enviar;

    public GenerationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_generation, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        generations.clear();

        recyclerView = view.findViewById(R.id.recyclerPoke);
        searcher = view.findViewById(R.id.searcher);
        inserter = view.findViewById(R.id.inserter);
        layer = view.findViewById(R.id.layer);
        enviar = view.findViewById(R.id.send);


        enviar.setVisibility(View.GONE);
        inserter.setVisibility(View.GONE);
        layer.setVisibility(View.GONE);
        getGenerations();
        searcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inserter.setVisibility(View.VISIBLE);
                layer.setVisibility(View.VISIBLE);
                enviar.setVisibility(View.VISIBLE);
            }
        });
        //String pokeBuscado = inserter.getText().toString();

        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!inserter.getText().toString().equals("")) {
                    Intent i = new Intent(getContext(), PokedexActivity.class);
                    i.putExtra("searched", inserter.getText().toString().toLowerCase(Locale.ROOT));
                    startActivity(i);
                    inserter.setText("");

                }else{
                    Toast.makeText(getContext(), "escribe un Pokemon", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }


    class GenAdapter extends RecyclerView.Adapter<GenAdapter.MyViewHolder> {
        private final ArrayList<Generation> generations;
        private final Context context;

        public GenAdapter(ArrayList<Generation> generations, Context context) {
            this.generations = generations;
            this.context = context;
        }


        @NonNull
        @Override
        public GenAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new GenAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.generation_row, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull GenAdapter.MyViewHolder holder, int position) {
            Generation generation = generations.get(holder.getAdapterPosition());
            holder.imageView.setImageResource(generation.url);
            holder.textGen.setImageResource(generation.urlName);


            holder.rowGen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i;
                    if (holder.getAdapterPosition() == 8) {
                        i = new Intent(context, CustomdexActivity.class);
                    } else {
                        i = new Intent(context, PokedexActivity.class);
                        i.putExtra("name", generations.get(holder.getAdapterPosition()).name);
                    }
                    context.startActivity(i);
                }
            });

        }

        @Override
        public int getItemCount() {
            return generations.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private final ImageView imageView;
            private final ImageView textGen;
            private ConstraintLayout rowGen;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.gensprite);
                textGen = itemView.findViewById(R.id.nameGen);
                rowGen = itemView.findViewById(R.id.rowGen);
            }
        }



        }
    private void getGenerations() {
        generations.add(new Generation("Kanto", R.drawable.kanto, R.drawable.kantou));
        generations.add(new Generation("Johto", R.drawable.johto, R.drawable.joutou));
        generations.add(new Generation("Hoenn", R.drawable.hoenn, R.drawable.hoenne));
        generations.add(new Generation("Sinnoh", R.drawable.sinnoh, R.drawable.sinnohe));
        generations.add(new Generation("Teselia", R.drawable.teselia, R.drawable.tesolia));
        generations.add(new Generation("Kalos", R.drawable.kalos, R.drawable.kalose));
        generations.add(new Generation("Alola", R.drawable.alola, R.drawable.alelo));
        generations.add(new Generation("Galar", R.drawable.galar, R.drawable.galare));
        generations.add(new Generation("Custom", R.drawable.custome, R.drawable.pokelogu));

        genAdapter = new GenAdapter(generations, getContext());
        recyclerView.setAdapter(genAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }
}
