package com.example.customdex_m013;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.customdex_m013.costumModel.AbilitiesCostum;
import com.example.customdex_m013.costumModel.MovesCostum;
import com.example.customdex_m013.costumModel.StatsCostum;
import com.example.customdex_m013.databinding.ActivityCustomDetailBinding;
import com.example.customdex_m013.databinding.ActivityForumBinding;
import com.example.customdex_m013.model.Forum;
import com.example.customdex_m013.model.PokemonCustom;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class CustomDetail extends AppCompatActivity {

    private ActivityCustomDetailBinding binding;
    private MoveDetailAdapter adapter;
    String name, clase, description, weight, height, type1, type2, sprite, authorName, preEvolucion;
    ArrayList<MovesCostum> movesCostums;
    ArrayList<AbilitiesCostum> abilitiesCostums;
    ArrayList<StatsCostum> statsCostums;
    public FirebaseFirestore db = FirebaseFirestore.getInstance();
    public FirebaseAuth auth = FirebaseAuth.getInstance();
    private CheckBox favorite;
    PokemonCustom pokemon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((binding = ActivityCustomDetailBinding.inflate(getLayoutInflater())).getRoot());

        getData();

        setData();
    }

    class MoveDetailAdapter extends RecyclerView.Adapter<MoveDetailAdapter.MyViewHolder> {

        private ArrayList<MovesCostum> movesCustom;
        private Context context;

        public MoveDetailAdapter(ArrayList<MovesCostum> movesCustom, Context context) {
            this.movesCustom = movesCustom;
            this.context = context;
        }

        @NonNull
        @Override
        public MoveDetailAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.move_row, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MoveDetailAdapter.MyViewHolder holder, int position) {
            MovesCostum moves = movesCustom.get(holder.getAdapterPosition());
            holder.movname.setText(moves.moveName);
            holder.movLvl.setText("lvl " + moves.moveLvl);
        }

        @Override
        public int getItemCount() {
            return movesCustom.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            final TextView movname;
            final ImageView movMethod;
            final TextView movLvl;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                movname = itemView.findViewById(R.id.moveName);
                movMethod = itemView.findViewById(R.id.tutor);
                movLvl = itemView.findViewById(R.id.lvlLearne);
            }
        }
    }

    public void getData() {
        try {
            name = getIntent().getStringExtra("name");
            authorName = getIntent().getStringExtra("authorName");
            clase = getIntent().getStringExtra("clase");
            description = getIntent().getStringExtra("description");
            weight = getIntent().getStringExtra("weight");
            height = getIntent().getStringExtra("height");
            type1 = getIntent().getStringExtra("type1");
            type2 = getIntent().getStringExtra("type2");
            sprite = getIntent().getStringExtra("sprite");
            movesCostums = (ArrayList<MovesCostum>) getIntent().getSerializableExtra("moves");
            abilitiesCostums = (ArrayList<AbilitiesCostum>) getIntent().getSerializableExtra("abilities");
            statsCostums = (ArrayList<StatsCostum>) getIntent().getSerializableExtra("stats");
            preEvolucion = getIntent().getStringExtra("preEvolution");
            pokemon = (PokemonCustom) getIntent().getSerializableExtra("pok");
        }catch (Exception e){
        }
        try{
            PokemonCustom pok = (PokemonCustom) getIntent().getSerializableExtra("pokemon");
            name = pok.name;
            authorName = pok.authorName;
            clase = pok.classe;
            description = pok.description;
            weight = pok.weight;
            height = pok.height;
            type1 = pok.type1;
            type2 = pok.type2;
            sprite = pok.sprite;
            movesCostums = pok.moves;
            abilitiesCostums = pok.abilities;
            statsCostums = pok.stats;
            pokemon = pok;
            preEvolucion = pok.preEvolution;
        }catch (Exception w){

        }

        favorite = findViewById(R.id.checkBox);
        favorite.setOnClickListener(v -> {
            db.collection("pokecustom").document(pokemon.id)
                    .update("favorite." + auth.getUid(), !pokemon.favorite.containsKey(auth.getUid()) ? true : FieldValue.delete());

        });
        try {
            favorite.setChecked(pokemon.favorite.containsKey(auth.getUid()));
        }catch (Exception e){

        }
    }

    public void setData() {
        binding.customDetailName.setText(name);
        binding.authorName.setText("Autor: " + authorName);
        binding.customDetailClass.setText(clase);
        binding.customDetailDesc.setText(description);
        binding.preEvolucion.setText(preEvolucion);
        binding.customDetailAltura.setText(height + " m");
        binding.customDetailPeso.setText(weight + " kg");
        Picasso.get().load(sprite).centerCrop().fit().into(binding.customDetailSprite);

    if (type1.equals("Acero")) binding.customDetailTipo1.setImageResource(R.drawable.acero);
    if (type1.equals("Agua")) binding.customDetailTipo1.setImageResource(R.drawable.agua);
    if (type1.equals("Bicho")) binding.customDetailTipo1.setImageResource(R.drawable.bicho);
    if (type1.equals("Dragon")) binding.customDetailTipo1.setImageResource(R.drawable.dragon);
    if (type1.equals("Electrico"))
        binding.customDetailTipo1.setImageResource(R.drawable.electrico);
    if (type1.equals("Fantasma"))
        binding.customDetailTipo1.setImageResource(R.drawable.fantasma);
    if (type1.equals("Fuego")) binding.customDetailTipo1.setImageResource(R.drawable.fuego);
    if (type1.equals("Hada")) binding.customDetailTipo1.setImageResource(R.drawable.hada);
    if (type1.equals("Hielo")) binding.customDetailTipo1.setImageResource(R.drawable.hielo);
    if (type1.equals("Lucha")) binding.customDetailTipo1.setImageResource(R.drawable.lucha);
    if (type1.equals("Normal")) binding.customDetailTipo1.setImageResource(R.drawable.normal);
    if (type1.equals("Planta")) binding.customDetailTipo1.setImageResource(R.drawable.planta);
    if (type1.equals("Psíquico"))
        binding.customDetailTipo1.setImageResource(R.drawable.psiquico);
    if (type1.equals("Roca")) binding.customDetailTipo1.setImageResource(R.drawable.roca);
    if (type1.equals("Siniestro"))
        binding.customDetailTipo1.setImageResource(R.drawable.siniestro);
    if (type1.equals("Tierra")) binding.customDetailTipo1.setImageResource(R.drawable.tierra);
    if (type1.equals("Veneno")) binding.customDetailTipo1.setImageResource(R.drawable.veneno);
    if (type1.equals("Volador")) binding.customDetailTipo1.setImageResource(R.drawable.volador);


    if (type2.equals("Acero")) binding.customDetailTipo2.setImageResource(R.drawable.acero);
    if (type2.equals("Agua")) binding.customDetailTipo2.setImageResource(R.drawable.agua);
    if (type2.equals("Bicho")) binding.customDetailTipo2.setImageResource(R.drawable.bicho);
    if (type2.equals("Dragon")) binding.customDetailTipo2.setImageResource(R.drawable.dragon);
    if (type2.equals("Electrico"))
        binding.customDetailTipo2.setImageResource(R.drawable.electrico);
    if (type2.equals("Fantasma"))
        binding.customDetailTipo2.setImageResource(R.drawable.fantasma);
    if (type2.equals("Fuego")) binding.customDetailTipo2.setImageResource(R.drawable.fuego);
    if (type2.equals("Hada")) binding.customDetailTipo2.setImageResource(R.drawable.hada);
    if (type2.equals("Hielo")) binding.customDetailTipo2.setImageResource(R.drawable.hielo);
    if (type2.equals("Lucha")) binding.customDetailTipo2.setImageResource(R.drawable.lucha);
    if (type2.equals("Normal")) binding.customDetailTipo2.setImageResource(R.drawable.normal);
    if (type2.equals("Planta")) binding.customDetailTipo2.setImageResource(R.drawable.planta);
    if (type2.equals("Psíquico"))
        binding.customDetailTipo2.setImageResource(R.drawable.psiquico);
    if (type2.equals("Roca")) binding.customDetailTipo2.setImageResource(R.drawable.roca);
    if (type2.equals("Siniestro"))
        binding.customDetailTipo2.setImageResource(R.drawable.siniestro);
    if (type2.equals("Tierra")) binding.customDetailTipo2.setImageResource(R.drawable.tierra);
    if (type2.equals("Veneno")) binding.customDetailTipo2.setImageResource(R.drawable.veneno);
    if (type2.equals("Volador")) binding.customDetailTipo2.setImageResource(R.drawable.volador);
    if (type2.equals("")) binding.customDetailTipo2.setVisibility(View.GONE);


    for (StatsCostum s : statsCostums) {
        binding.customDetailAtk.setText(s.attack);
        binding.customDetailAtksp.setText(s.specialAttack);
        binding.customDetailDef.setText(s.defense);
        binding.customDetailDefsp.setText(s.specialDefense);
        binding.customDetailHP.setText(s.healthPoints);
        binding.customDetailVel.setText(s.velocity);
    }

    for (AbilitiesCostum a : abilitiesCostums) {
        binding.customDetailHabilidad1.setText(a.ability1);
        binding.customDetailHabilidad3.setText(a.ability0);
        try {
            binding.customDetailHabilidad2.setText(a.ability2);
        } catch (Exception e) {
            System.out.println("No tiene 2 habilidad");
        }
    }

        adapter = new MoveDetailAdapter(movesCostums, this);
        binding.customDetailMoves.setAdapter(adapter);
        binding.customDetailMoves.setLayoutManager(new LinearLayoutManager(this));
        int atk, spatk, def, spdef, vel, hp;
        int total;

        for(StatsCostum s : statsCostums) {
            atk = Integer.parseInt(s.attack);
            spatk = Integer.parseInt(s.specialAttack);
            def = Integer.parseInt(s.defense);
            spdef = Integer.parseInt(s.specialDefense);
            vel = Integer.parseInt(s.velocity);
            hp = Integer.parseInt(s.healthPoints);
            total = atk + spatk + def + spdef + vel + hp;
            binding.customDetailTotal.setText(total + "");
        }
        movesCostums.remove(movesCostums.size()-1);
        Collections.sort(movesCostums, new SortMoveByLevel());
    }

    static class SortMoveByLevel implements Comparator<MovesCostum> {

        @Override
        public int compare(MovesCostum move, MovesCostum move2) {
            return move.id.compareTo(move2.id);
        }

        @Override
        public boolean equals(Object o) {
            return false;
        }
    }
}