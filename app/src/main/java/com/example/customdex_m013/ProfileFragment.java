package com.example.customdex_m013;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.customdex_m013.model.PokemonCustom;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.storage.FirebaseStorage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;
import java.util.UUID;

public class ProfileFragment extends Fragment {
    public Button confirm, cambiafoto, confName;
    private Uri urimage;
    public ImageView image, namechanger;
    public TextView name, mail;
    public FirebaseAuth auth = FirebaseAuth.getInstance();
    public AppViewModel appViewModel;
    public FirebaseStorage storage = FirebaseStorage.getInstance();
    public TextInputLayout nameLayout;
    public TextInputEditText nameNew;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ProfAdapter myAdapter;
    private RecyclerView recyclerView;
    private ArrayList<PokemonCustom> profilepoke = new ArrayList<>();
    public ProfileFragment() {
        // Required empty public constructor
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        appViewModel = new ViewModelProvider(requireActivity()).get(AppViewModel.class);

        confirm = view.findViewById(R.id.confirm);
        cambiafoto = view.findViewById(R.id.cambiafoto);
        image = view.findViewById(R.id.previo);
        name = view.findViewById(R.id.Nombre);
        namechanger = view.findViewById(R.id.nameChanger);
        confName = view.findViewById(R.id.confirmName);
        nameLayout = view.findViewById(R.id.newNameLayout);
        nameNew = view.findViewById(R.id.newName);
        recyclerView = view.findViewById(R.id.recyclerCustom);

        confirm.setVisibility(View.INVISIBLE);
        nameLayout.setVisibility(View.GONE);
        confName.setVisibility(View.INVISIBLE);
        confName.setEnabled(false);
        confName.setOnClickListener(z ->{
            FirebaseUser user = auth.getCurrentUser();
            UserProfileChangeRequest change = new UserProfileChangeRequest.Builder()
                    .setDisplayName(nameNew.getText().toString()).build();

        });



        setQuery().addSnapshotListener((collectionSnapshot, e)->{
            profilepoke.clear();
            for(DocumentSnapshot snap :collectionSnapshot){
                PokemonCustom pok = snap.toObject(PokemonCustom.class);
                pok.id = snap.getId();
                profilepoke.add(pok);
            }
            myAdapter.notifyDataSetChanged();
        });
        myAdapter = new ProfAdapter(profilepoke, getContext());
        recyclerView.setAdapter(myAdapter);

        name.setText(auth.getCurrentUser().getDisplayName());
        Glide.with(this).load(auth.getCurrentUser().getPhotoUrl()).into(image);
        cambiafoto.setOnClickListener(v -> {galeria.launch("image/*"); });

        appViewModel.uriImagenSeleccionada.observe(getViewLifecycleOwner(), uri -> {
            if (uri != null) {
                Glide.with(this).load(uri).into(image);
                urimage = uri;
                confirm.setVisibility(View.VISIBLE);
            }
        });
        name.setText(auth.getCurrentUser().getDisplayName());

        confirm.setOnClickListener(v -> {
            confirm.setEnabled(false);
            storage.getReference("/images/"+ UUID.randomUUID()+".jpg")
                    .putFile(urimage)
                    .continueWithTask(task -> task.getResult().getStorage().getDownloadUrl()
                            .addOnSuccessListener(urlDesc ->{
                                FirebaseUser user = auth.getCurrentUser();
                                UserProfileChangeRequest profileChangeRequest =  new UserProfileChangeRequest.Builder()
                                        .setPhotoUri(Uri.parse(urlDesc.toString())).build();
                                user.updateProfile(profileChangeRequest)
                                        .addOnCompleteListener(task1 -> {
                                            if(task1.isSuccessful()){
                                                Log.d("asdasdad", "userupdated");
                                            }
                                        });
                            }));
        });
    }

    private final ActivityResultLauncher<String> galeria = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        appViewModel.setUriImagenSeleccionada(uri);
    });


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    Query setQuery() { return db.collection("pokecustom").whereEqualTo("authorName", FirebaseAuth.getInstance().getCurrentUser().getDisplayName()); }



    public class ProfAdapter extends RecyclerView.Adapter<ProfileFragment.ProfAdapter.MyViewHolder>{
        private final ArrayList<PokemonCustom> pokemons;
        private final Context context;

        public ProfAdapter(ArrayList<PokemonCustom> pokemons, Context context) {
            this.pokemons = pokemons;
            this.context = context;
        }

        @NonNull
        @Override
        public ProfileFragment.ProfAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ProfAdapter.MyViewHolder(LayoutInflater.from(context).inflate(R.layout.pokemon_row, parent, false));

        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
            PokemonCustom pokemon = pokemons.get(holder.getAdapterPosition());
            Picasso.get().load(pokemon.sprite).fit().centerCrop().into(holder.imageView);
            holder.textName.setText(pokemon.name.toUpperCase(Locale.ROOT));
            holder.constraintLayout.setOnClickListener(intent -> {
                Intent i = new Intent(context, CustomDetail.class);
                i.putExtra("pokemon", pokemon);
                startActivity(i);
            });
        }

        @Override
        public int getItemCount() {
            return pokemons.size();

        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;
            TextView textName;
            ConstraintLayout constraintLayout;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.poksprite);
                textName = itemView.findViewById(R.id.pokename);
                constraintLayout = itemView.findViewById(R.id.thepokerow);
            }
        }
    }
}