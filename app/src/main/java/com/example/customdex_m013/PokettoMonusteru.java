package com.example.customdex_m013;

public class PokettoMonusteru {
    String name;
    String image;
    String type1;
    String type2;

    public PokettoMonusteru() {
    }

    public PokettoMonusteru(String name, String image, String type1, String type2) {
        this.name = name;
        this.image = image;
        this.type1 = type1;
        this.type2 = type2;
    }
}
