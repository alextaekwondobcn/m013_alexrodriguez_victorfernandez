package com.example.customdex_m013;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.GsonBuilder;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

public class PokemonActivity extends AppCompatActivity {
    String namePoke;
    String pokeImage;
    String specieUrl;
    TextView namePokemon;
    ImageView imagePok;
    Specie sp1 = new Specie();
    TextView generus;
    SlotType type1;
    SlotType type2;
    int slotPos1;
    int slotPos2;
    String typeName1;
    String typeName2;
    ImageView type1Image;
    ImageView type2Image;
    double altura;
    TextView height;
    double peso;
    TextView weight;
    ImageView firstPokeChain;
    TextView nameFirstChain;
    Evolution_chain evolutionChainObj;
    String evoLink;
    EvolutionChain evolutionChain = new EvolutionChain();
    ImageView secondPokeChain, secondPokeChain2,secondPokeChain3,secondPokeChain4,secondPokeChain5,secondPokeChain6,secondPokeChain7,secondPokeChain8;
    TextView secondPokeName,secondPokeName2,secondPokeName3,secondPokeName4,secondPokeName5,secondPokeName6,secondPokeName7,secondPokeName8;
    CardView evochan, varitety;
    ImageView thirdPokeChain, thirdPokeChainAlt;
    TextView thirsPokeName, thirdPokeNameAlt;
    Specie specie = new Specie();
    int idForImages;
    Pokemon pokemon = new Pokemon();
    String url2ndPoke, url2ndPokeAlt,url2ndPokeAlt1, url2ndPokeAlt2, url2ndPokeAlt3, url2ndPokeAlt4,url2ndPokeAlt5,url2ndPokeAlt6 ;
    String url3rdEvo, url3rdEvoAlt;
    Specie specieevo1 = new Specie();
    Specie specieevo1Alt = new Specie();
    Specie specieevo1Alt1 = new Specie();
    Specie specieevo1Alt2 = new Specie();
    Specie specieevo1Alt3 = new Specie();
    Specie specieevo1Alt4 = new Specie();
    Specie specieevo1Alt5 = new Specie();
    Specie specieevo1Alt6 = new Specie();
    Pokemon movePoke =new Pokemon();
    int idForEvoImage, idForEvoImageAlt, idForEvoImageAlt1,idForEvoImageAlt2,idForEvoImageAlt3,idForEvoImageAlt4,idForEvoImageAlt5,idForEvoImageAlt6;
    TextView trigger,trigger2,trigger3,trigger4,trigger5,trigger6,trigger7,trigger8,trigger9,trigger1;
    TextView condicion1rstEvo,condicion1rstEvo1,condicion1rstEvo2,condicion1rstEvo3,condicion1rstEvo4,condicion1rstEvo5,condicion1rstEvo6,condicion1rstEvo7,condicion1rstEvo8,condicion1rstEvo9;
    TextView condicion2ndaEvo,condicion2ndaEvo1,condicion2ndaEvo2,condicion2ndaEvo3,condicion2ndaEvo4,condicion2ndaEvo5,condicion2ndaEvo6,condicion2ndaEvo7,condicion2ndaEvo8,condicion2ndaEvo9;
    String pokeapi="https://pokeapi.co/api/v2/pokemon/";

    Specie specieevo3 = new Specie();
    Specie specieevo3Alt = new Specie();

    int idFor3rdEvo, idFor3rdEvoAlt;

    String triggero1,triggero2,triggero3,triggero4,triggero5,triggero6,triggero7,triggero8,triggerFin1,triggerFin2;
    String cond1,cond2,cond3,cond4,cond5,cond6,cond7,cond8,cond9,cond10,cond11,cond12,cond13,cond14,cond15,cond16,condFin1,condFin2, condFin3,condFin4;
    String hab1, hab2, hab3, nameForOnClickTrial;
    TextView habil1, habil2, habiloc, noEvo;
    int hp, atk, def, spAtk, spDef, vel;
    TextView pv, atq, defe, spAtq, spDefe, speed, total;
    ImageView spFront, spBack,shFront, shBack;
    String spriteBack, spriteShony, backShiny;
    int id;
    String pokeInfo; TextView infoPokemon;
    ArrayList<Movimientos>movimes=new ArrayList<>();
    ArrayList<Movimientos> ordenaMoves = new ArrayList<>();
    ArrayList<Movimientos> movestuto= new ArrayList<>();
    MoveAdapter moveAdapter;
    RecyclerView reciclaMoves, cyclerVariety;
    VarietyAdapter varietyAdapter;
    ImageView metodo;
    ImageView frontFemale, backFemale, shinyFemale, shinyBackFemale;
    String frontFem, backFem, shiFem, shiBackFem; TextView ffemaleText, bfemaleText, sfemaleText, sbfemaleText;
    ArrayList<PoketMonster> pokeVarieties = new ArrayList<>();
    ArrayList<PokettoMonusteru> varieties = new ArrayList<>();
    ConstraintLayout pokonstraint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pokemon);

try {
    namePoke = getIntent().getStringExtra("name").toUpperCase(Locale.ROOT);
    pokeImage = getIntent().getStringExtra("sprite");
    specieUrl = getIntent().getStringExtra("specie");
    typeName1 = getIntent().getStringExtra("type_name0");
    altura = getIntent().getIntExtra("altura", 1);
    peso = getIntent().getIntExtra("peso", 1);

    spriteBack = getIntent().getStringExtra("spriteBack");
    spriteShony = getIntent().getStringExtra("spriteShiny");
    backShiny = getIntent().getStringExtra("backShiny");
    id = getIntent().getIntExtra("id", 0);
}catch (Exception w){
    nameForOnClickTrial = getIntent().getStringExtra("nametrial");
    System.out.println(specieUrl);
    System.out.println(nameForOnClickTrial);
    nameForOnClickTrial = nameForOnClickTrial.toLowerCase(Locale.ROOT);
    getPokeRefillInfo();

}
        double pesofinal = peso / 10;
        double altfinal = altura / 10;
        try {
            slotPos2 = getIntent().getIntExtra("slot_num1",1);
            typeName2 = getIntent().getStringExtra("type_name1");
            hab1 = getIntent().getStringExtra("habilidad1");
            hab2 = getIntent().getStringExtra("habilidad2");
            hab3 = getIntent().getStringExtra("habilidadoculta");
            hp = getIntent().getIntExtra("hp",0);
            atk = getIntent().getIntExtra("atk",0);
            def = getIntent().getIntExtra("def", 0);
            spAtk = getIntent().getIntExtra("spAtk",0);
            spDef = getIntent().getIntExtra("spDef",0);
            vel = getIntent().getIntExtra("speed",0);
        }catch (Exception e){
            System.out.println("no hay");
        }


        frontFemale = findViewById(R.id.spritefront2);
        backFemale = findViewById(R.id.spriteback2);
        shinyFemale = findViewById(R.id.shinyFem);
        shinyBackFemale =findViewById(R.id.shinyBackFem);
        ffemaleText = findViewById(R.id.frFemaleText);
        bfemaleText=findViewById(R.id.BaFemaleText);
        sfemaleText=findViewById(R.id.ShiFemaleText);
        sbfemaleText=findViewById(R.id.BaShiFemaleText);
        try{
            frontFem = getIntent().getStringExtra("frontFemale");
            backFem = getIntent().getStringExtra("backFemale");
            shiFem = getIntent().getStringExtra("shinyFemale");
            shiBackFem = getIntent().getStringExtra("backShinyFemale");
        }catch (Exception e){

        }
        if(frontFemale!=null){
            frontFemale.setVisibility(View.GONE);
            backFemale.setVisibility(View.GONE);
            shinyFemale.setVisibility(View.GONE);
            shinyBackFemale.setVisibility(View.GONE);
            ffemaleText.setVisibility(View.GONE);
            bfemaleText.setVisibility(View.GONE);
            sfemaleText.setVisibility(View.GONE);
            sbfemaleText.setVisibility(View.GONE);
        }


        namePokemon = findViewById(R.id.customDetailName);
        imagePok = findViewById(R.id.customDetailSprite);
        generus = findViewById(R.id.customDetailClass);
        type1Image = findViewById(R.id.customDetailTipo1);
        type2Image = findViewById(R.id.customDetailTipo2);
        height = findViewById(R.id.customDetailAltura);
        weight = findViewById(R.id.customDetailPeso);
        firstPokeChain = findViewById(R.id.firstEvo);
        nameFirstChain = findViewById(R.id.nameEvo1);
        secondPokeChain = findViewById(R.id.secondEvo);
        secondPokeChain2 = findViewById(R.id.secondEvoAlt);
        secondPokeChain3 = findViewById(R.id.secondEvoAltDos);
        secondPokeChain4 = findViewById(R.id.secondEvoAltTres);
        secondPokeChain5 = findViewById(R.id.secondEvoAltCuatro);
        secondPokeChain6 = findViewById(R.id.secondEvoAltCinco);
        secondPokeChain7 = findViewById(R.id.secondEvoAltSeis);
        secondPokeChain8 = findViewById(R.id.secondEvoAltSiete);
        secondPokeName = findViewById(R.id.nameEvo2);
        secondPokeName2 = findViewById(R.id.nameEvo2Alt);
        secondPokeName3 = findViewById(R.id.nameEvo2AltSec);
        secondPokeName4 = findViewById(R.id.nameEvo2AltThird);
        secondPokeName5 = findViewById(R.id.nameEvo2AltFourth);

        secondPokeName6 = findViewById(R.id.nameEvo2AltFifth);
        secondPokeName7 = findViewById(R.id.nameEvo2AltSixth);
        secondPokeName8 = findViewById(R.id.nameEvo2AltSeventh);
        evochan = findViewById(R.id.evochan);
        thirdPokeChain = findViewById(R.id.thirdEvo);
        thirdPokeChainAlt = findViewById(R.id.thirdEvoAlt);
        thirsPokeName = findViewById(R.id.nameEvo3);
        thirdPokeNameAlt = findViewById(R.id.nameEvo3Alt);
        //hooks triggers
        trigger = findViewById(R.id.trigger);
        trigger2 = findViewById(R.id.triggerEvo2);
        trigger3 = findViewById(R.id.triggerEvo2Alt);
        trigger4 = findViewById(R.id.triggerEvo2Alt1);
        trigger5 = findViewById(R.id.triggerEvo2Alt2);
        trigger6 = findViewById(R.id.triggerEvo2Alt3);
        trigger7 = findViewById(R.id.triggerEvo2Alt4);
        trigger8 = findViewById(R.id.triggerEvo2Alt5);

        trigger9 = findViewById(R.id.trigger3);
        trigger1 = findViewById(R.id.triggerEvo5);
        //hooks condicion1
        condicion1rstEvo = findViewById(R.id.condicion1);
        condicion1rstEvo1 = findViewById(R.id.condicionEvo1);
        condicion1rstEvo2 = findViewById(R.id.CondicionEvo1Alt);
        condicion1rstEvo3 = findViewById(R.id.condicionEvoAlt1);
        condicion1rstEvo4 = findViewById(R.id.condicionEvoAlt2);
        condicion1rstEvo5 = findViewById(R.id.condicionEvoAlt3);
        condicion1rstEvo6 = findViewById(R.id.condicionEvoAlt4);
        condicion1rstEvo7 = findViewById(R.id.condicionEvoAlt5);

        condicion1rstEvo8 = findViewById(R.id.condicion);
        condicion1rstEvo9 = findViewById(R.id.condicionEvo3);
        //hooks condicion2
        condicion2ndaEvo = findViewById(R.id.condicion2);
        condicion2ndaEvo1 = findViewById(R.id.condicionEvo2);
        condicion2ndaEvo2 = findViewById(R.id.condicionEvo2Alt);
        condicion2ndaEvo3 = findViewById(R.id.condicionEvo2Alt1);
        condicion2ndaEvo4 = findViewById(R.id.condicionEvo2Alt2);
        condicion2ndaEvo5 = findViewById(R.id.condicionEvo2Alt3);
        condicion2ndaEvo6 = findViewById(R.id.condicionEvo2Alt5);
        condicion2ndaEvo7 = findViewById(R.id.condicionEvo2Alt6);

        condicion2ndaEvo8 = findViewById(R.id.condicion3);
        condicion2ndaEvo9 = findViewById(R.id.condicionEvo4);

        habil1 = findViewById(R.id.customDetailHabilidad1);
        habil2 = findViewById(R.id.customDetailHabilidad2);
        habiloc = findViewById(R.id.customDetailHabilidad3);

        pv = findViewById(R.id.customDetailHP);
        atq = findViewById(R.id.customDetailAtk);
        defe = findViewById(R.id.customDetailDef);
        spAtq = findViewById(R.id.customDetailAtksp);
        spDefe = findViewById(R.id.customDetailDefsp);
        speed = findViewById(R.id.customDetailVel);
        total = findViewById(R.id.customDetailTotal);
        spFront=findViewById(R.id.spritefront);
        spBack=findViewById(R.id.spriteback);
        shFront=findViewById(R.id.spriteShiny);
        shBack=findViewById(R.id.spriteShiny2);
        infoPokemon = findViewById(R.id.customDetailDesc);
        reciclaMoves = findViewById(R.id.customDetailMoves);
        cyclerVariety = findViewById(R.id.varieCycler);
        varitety = findViewById(R.id.varieties);
        pokonstraint = findViewById(R.id.pokeConstraint);


        getSpecie();
        typeValues();


        namePokemon.setText(namePoke);
        Picasso.get().load(pokeImage).fit().centerCrop().into(imagePok);
        firstPokeChain.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", nameFirstChain.getText().toString());
            finish();
            startActivity(i);
        });

        secondPokeChain.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", secondPokeName.getText().toString());
            finish();
            startActivity(i);
        });
        secondPokeChain2.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", secondPokeName2.getText().toString());
            finish();
            startActivity(i);
        });
        secondPokeChain3.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", secondPokeName3.getText().toString());
            finish();
            startActivity(i);
        });
        secondPokeChain4.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", secondPokeName4.getText().toString());
            finish();
            startActivity(i);
        });
        secondPokeChain5.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", secondPokeName5.getText().toString());
            finish();
            startActivity(i);
        });
        secondPokeChain6.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", secondPokeName6.getText().toString());
            finish();
            startActivity(i);
        });
        secondPokeChain7.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", secondPokeName7.getText().toString());
            finish();
            startActivity(i);
        });
        secondPokeChain8.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", secondPokeName8.getText().toString());
            finish();
            startActivity(i);
        });
        thirdPokeChain.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", thirsPokeName.getText().toString());
            finish();
            startActivity(i);
        });
        thirdPokeChainAlt.setOnClickListener(v -> {
            Intent i = new Intent(this, PokemonActivity.class);
            i.putExtra("nametrial", thirdPokeNameAlt.getText().toString());
            finish();
            startActivity(i);

        });



try {
    habil1.setText(hab1.substring(0, 1).toUpperCase() + hab1.substring(1));
}catch (Exception e){

}
    try {
        habil2.setText(hab2.substring(0, 1).toUpperCase() + hab2.substring(1));
    } catch (Exception e) {
        habil2.setText("");
    }
    try {
        if (hab3 == null) {
            habiloc.setText(hab1.substring(0, 1).toUpperCase() + hab1.substring(1));
        } else {
            habiloc.setText(hab3.substring(0, 1).toUpperCase() + hab3.substring(1));
        }

        pv.setText(hp + "");
        atq.setText(atk + "");
        defe.setText(def + "");
        spAtq.setText(spAtk + "");
        spDefe.setText(spDef + "");
        speed.setText(vel + "");
        int totalus = hp + atk + def + spAtk + spDef + vel;
        total.setText(totalus + "");
    }catch (Exception e){

    }


    height.setText(altfinal + " m");
    weight.setText(pesofinal + " Kg");
    Picasso.get().load(pokeImage).fit().centerCrop().into(spFront);
    Picasso.get().load(spriteBack).fit().centerCrop().into(spBack);
    Picasso.get().load(spriteShony).fit().centerCrop().into(shFront);
    Picasso.get().load(backShiny).fit().centerCrop().into(shBack);

        try {
            Picasso.get().load(frontFem).fit().centerCrop().into(frontFemale);
            Picasso.get().load(backFem).fit().centerCrop().into(backFemale);
            Picasso.get().load(shiFem).fit().centerCrop().into(shinyFemale);
            Picasso.get().load(shiBackFem).fit().centerCrop().into(shinyBackFemale);
        }catch (Exception e){

        }



    }

    private void getPokeRefillInfo() {
        Volley.newRequestQueue(this).add(new JsonObjectRequest(
                (Request.Method.GET),
                pokeapi+nameForOnClickTrial+"/",
                null,
                response -> {
                    JSONObject pok = response;
                    Pokemon poke = new GsonBuilder().create().fromJson(pok.toString(),Pokemon.class);
                    namePokemon.setText(poke.name.toUpperCase(Locale.ROOT));
                    Picasso.get().load(poke.sprites.front_default).fit().centerCrop().into(imagePok);
                    specieUrl = poke.species.url;
                    id = poke.id;
                    getSpecie();
                    for(SlotType type: poke.types){
                        if(type.slot==1){
                            typeName1 = type.type.name;
                        }else if(type.slot==2){
                            typeName2 = type.type.name;
                        }
                        typeValues();
                        double alturita = poke.height;
                        double altfinale = alturita/10;
                        double pesito = poke.weight;
                        double pesfinale = pesito/10;
                        height.setText(altfinale+" m");
                        weight.setText(pesfinale+" kg");

                        for(Abilities abi : poke.abilities){
                            if(abi.slot==1){
                                hab1 =abi.ability.name;
                                habil1.setText(hab1.substring(0, 1).toUpperCase() + hab1.substring(1));

                            }else if(abi.slot==2){
                                hab2= abi.ability.name;
                                try {
                                    habil2.setText(hab2.substring(0, 1).toUpperCase() + hab2.substring(1));
                                } catch (Exception e) {
                                    habil2.setText("");
                                }
                            }else if(abi.is_hidden){
                                hab3=abi.ability.name;
                                try {
                                    if (hab3 == null) {
                                        habiloc.setText(hab1.substring(0, 1).toUpperCase() + hab1.substring(1));
                                    } else {
                                        habiloc.setText(hab3.substring(0, 1).toUpperCase() + hab3.substring(1));
                                    }
                                }catch (Exception e){

                                }
                            }
                        }
                        for(Stats stat :poke.stats) {
                            if(stat.stat.name.equals("hp")){
                                hp = stat.base_stat;
                                pv.setText(hp + "");
                            }else if(stat.stat.name.equals("attack")){
                                atk = stat.base_stat;
                                atq.setText(atk + "");
                            }else if(stat.stat.name.equals("defense")){
                                def = stat.base_stat;
                                defe.setText(def + "");
                            }else if(stat.stat.name.equals("special-attack")){
                                spAtk = stat.base_stat;
                                spAtq.setText(spAtk + "");
                            }else if(stat.stat.name.equals("special-defense")){
                                spDef = stat.base_stat;
                                spDefe.setText(spDef + "");
                            }else if(stat.stat.name.equals("speed")){
                                vel = stat.base_stat;
                                speed.setText(vel + "");
                            }

                            int totalus = hp+atk+def+spAtk+spDef+vel;
                            total.setText(totalus + "");
                        }
                        Picasso.get().load(poke.sprites.front_default).fit().centerCrop().into(spFront);
                        Picasso.get().load(poke.sprites.back_default).fit().centerCrop().into(spBack);
                        Picasso.get().load(poke.sprites.front_shiny).fit().centerCrop().into(shFront);
                        Picasso.get().load(poke.sprites.back_shiny).fit().centerCrop().into(shBack);
                        try {
                            Picasso.get().load(poke.sprites.front_female).fit().centerCrop().into(frontFemale);
                            Picasso.get().load(poke.sprites.back_female).fit().centerCrop().into(backFemale);
                            Picasso.get().load(poke.sprites.front_shiny_female).fit().centerCrop().into(shinyFemale);
                            Picasso.get().load(poke.sprites.back_shiny_female).fit().centerCrop().into(shinyBackFemale);
                        }catch (Exception e){

                        }



                    }

                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())
        ));

    }

    private void getVarieties(){
        if(sp1.varieties.size()>1) {
            for (Varieties var : sp1.varieties) {
                if (!var.is_default) {
                    pokeVarieties.add(var.pokemon);
                }

            }
            for (PoketMonster poke : pokeVarieties) {
                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                        Request.Method.GET,
                        poke.url,
                        null,
                        response -> {
                            JSONObject vary = response;
                            Pokemon pokeVa = new GsonBuilder().create().fromJson(vary.toString(), Pokemon.class);
                            String ty1 = "";
                            String ty2 = "";
                            for (SlotType ty : pokeVa.types) {

                                if (ty.slot == 1) {
                                    ty1 = ty.type.name;
                                } else if (ty.slot == 2) {
                                    ty2 = ty.type.name;
                                }


                            }
                            String numbrePok =  poke.name.substring(0,1).toUpperCase()+poke.name.substring(1);
                            String numfin = numbrePok.replaceAll("-", " ");


                            PokettoMonusteru pok1 = new PokettoMonusteru(numfin, pokeVa.sprites.front_default, ty1, ty2);
                            varieties.add(pok1);
                            cyclerVariety.setLayoutManager(new GridLayoutManager(this, 2, RecyclerView.VERTICAL, false));
                            varietyAdapter = new VarietyAdapter(varieties, getApplicationContext());
                            cyclerVariety.setAdapter(varietyAdapter);

                        },
                        error -> Log.d("tag", "onErrorResponse: " + error.getMessage())


                ));
            }


        }else{
            varitety.setVisibility(View.GONE);
        }
    }
    class VarietyAdapter extends RecyclerView.Adapter<VarietyAdapter.MyViewHolder>{
        private ArrayList<PokettoMonusteru>pokis;
        private Context context;

        public VarietyAdapter(ArrayList<PokettoMonusteru> pokis, Context cont) {
            this.pokis = pokis;
            this.context = cont;
        }

        @NonNull
        @Override
        public VarietyAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.variety_row, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull VarietyAdapter.MyViewHolder holder, int position) {
            PokettoMonusteru pok = pokis.get(holder.getAdapterPosition());
            if(pok.image!=null) {
                Picasso.get().load(pok.image).fit().centerCrop().into(holder.varieImage);
            }else{
                holder.varieImage.setImageResource(R.drawable.pokeball);
            }

            if (pok.type1.equals("normal")) {
                holder.tipus1.setImageResource(R.drawable.normal);
            } else if (pok.type1.equals("fighting")) {
                holder.tipus1.setImageResource(R.drawable.lucha);

            } else if (pok.type1.equals("flying")) {
                holder.tipus1.setImageResource(R.drawable.volador);

            } else if (pok.type1.equals("poison")) {
                holder.tipus1.setImageResource(R.drawable.veneno);

            } else if (pok.type1.equals("ground")) {
                holder.tipus1.setImageResource(R.drawable.tierra);

            } else if (pok.type1.equals("rock")) {
                holder.tipus1.setImageResource(R.drawable.roca);

            } else if (pok.type1.equals("bug")) {
                holder.tipus1.setImageResource(R.drawable.bicho);

            } else if (pok.type1.equals("ghost")) {
                holder.tipus1.setImageResource(R.drawable.fantasma);

            } else if (pok.type1.equals("steel")) {
                holder.tipus1.setImageResource(R.drawable.acero);

            } else if (pok.type1.equals("fire")) {
                holder.tipus1.setImageResource(R.drawable.fuego);

            } else if (pok.type1.equals("water")) {
                holder.tipus1.setImageResource(R.drawable.agua);

            } else if (pok.type1.equals("grass")) {
                holder.tipus1.setImageResource(R.drawable.planta);

            } else if (pok.type1.equals("electric")) {
                holder.tipus1.setImageResource(R.drawable.electrico);

            } else if (pok.type1.equals("psychic")) {
                holder.tipus1.setImageResource(R.drawable.psiquico);

            } else if (pok.type1.equals("ice")) {
                holder.tipus1.setImageResource(R.drawable.hielo);

            } else if (pok.type1.equals("dragon")) {
                holder.tipus1.setImageResource(R.drawable.dragon);

            } else if (pok.type1.equals("dark")) {
                holder.tipus1.setImageResource(R.drawable.siniestro);

            } else if (pok.type1.equals("fairy")) {
                holder.tipus1.setImageResource(R.drawable.hada);

            }

            try {
                System.out.println("hay2tipo");
                if (pok.type2.equals("normal")) {
                    holder.tipus2.setImageResource(R.drawable.normal);
                } else if (pok.type2.equals("fighting")) {
                    holder.tipus2.setImageResource(R.drawable.lucha);

                } else if (pok.type2.equals("flying")) {
                    holder.tipus2.setImageResource(R.drawable.volador);

                } else if (pok.type2.equals("poison")) {
                    holder.tipus2.setImageResource(R.drawable.veneno);

                } else if (pok.type2.equals("ground")) {
                    holder.tipus2.setImageResource(R.drawable.tierra);

                } else if (pok.type2.equals("rock")) {
                    holder.tipus2.setImageResource(R.drawable.roca);

                } else if (pok.type2.equals("bug")) {
                    holder.tipus2.setImageResource(R.drawable.bicho);

                } else if (pok.type2.equals("ghost")) {
                    holder.tipus2.setImageResource(R.drawable.fantasma);

                } else if (pok.type2.equals("steel")) {
                    holder.tipus2.setImageResource(R.drawable.acero);

                } else if (pok.type2.equals("fire")) {
                    holder.tipus2.setImageResource(R.drawable.fuego);

                } else if (pok.type2.equals("water")) {
                    holder.tipus2.setImageResource(R.drawable.agua);

                } else if (pok.type2.equals("grass")) {
                    holder.tipus2.setImageResource(R.drawable.planta);

                } else if (pok.type2.equals("electric")) {
                    holder.tipus2.setImageResource(R.drawable.electrico);

                } else if (pok.type2.equals("psychic")) {
                    holder.tipus2.setImageResource(R.drawable.psiquico);

                } else if (pok.type2.equals("ice")) {
                    holder.tipus2.setImageResource(R.drawable.hielo);

                } else if (pok.type2.equals("dragon")) {
                    holder.tipus2.setImageResource(R.drawable.dragon);

                } else if (pok.type2.equals("dark")) {
                    holder.tipus2.setImageResource(R.drawable.siniestro);

                } else if (pok.type2.equals("fairy")) {
                    holder.tipus2.setImageResource(R.drawable.hada);

                }
            } catch (Exception e) {
                holder.tipus2.setImageResource(0);
            }
            holder.varname.setText(pok.name);



        }

        @Override
        public int getItemCount() {
            return pokis.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            final ImageView varieImage;
            final ImageView tipus1;
            final ImageView tipus2;
            final TextView varname;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                varieImage = itemView.findViewById(R.id.varieImage);
                tipus1 = itemView.findViewById(R.id.varieType1);
                tipus2 = itemView.findViewById(R.id.varieType2);
                varname = itemView.findViewById(R.id.varieName);
            }
        }
    }




    private void getSpecie(){

        Volley.newRequestQueue(this).add(new JsonObjectRequest(
                Request.Method.GET,
                specieUrl,
                null,
                response -> {
                    JSONObject specie1 = response;
                    sp1 = new GsonBuilder().create().fromJson(specie1.toString(), Specie.class);

                    generus.setText(getEsGenera(sp1));
                    evolutionChainObj = sp1.evolution_chain;
                    System.out.println(evolutionChainObj.url);
                    evoLink = evolutionChainObj.url;
                    getEvolutionChain();
                    getPokeInfo();
                    getMoves();
                    getVarieties();

                },
                error -> Log.d("tag", "onErrorResponse: " + error.getMessage())

        ));
    }
    public void getMoves() {

        Volley.newRequestQueue(this).add(new JsonObjectRequest(
                Request.Method.GET,
                "https://pokeapi.co/api/v2/pokemon/"+id+"/",
                null,
                response -> {
                    JSONObject pokeObject = response;
                    String lernmeth="hehe";
                    int lvllearnt=0;
                    Pokemon pokecosa = new GsonBuilder().create().fromJson(pokeObject.toString(), Pokemon.class);
                    System.out.println(pokecosa.name);
                    for (Moves moves : pokecosa.moves) {
                        for(Version_group_details ver : moves.version_group_details){
                            lvllearnt = ver.level_learned_at;
                            lernmeth=ver.move_learn_method.name;

                        }
                        String movsubs = moves.move.name.replaceAll("-", " ");

                        Movimientos movi = new Movimientos(movsubs, lernmeth, lvllearnt);
                        if(movi.levelLearnt>0){
                            ordenaMoves.add(movi);
                        }else if(movi.methodLearnName.equals("machine")){
                            movimes.add(movi);
                        }else if(movi.methodLearnName.equals("tutor")){
                            movestuto.add(movi);
                        }
                    }
                    ordenaMoves.sort(Comparator.comparingInt(moves -> moves.levelLearnt));
                    for(Movimientos mv : movimes){
                        ordenaMoves.add(mv);
                    }
                    for(Movimientos mmv : movestuto){
                        ordenaMoves.add(mmv);
                    }



                    reciclaMoves.setLayoutManager(new LinearLayoutManager(this));
                    moveAdapter = new MoveAdapter(ordenaMoves,getApplicationContext());
                    reciclaMoves.setAdapter(moveAdapter);


                },
                error -> Log.d("tag", "onErrorResponse: " + "ERROR: " + error.getMessage())
        ));



    }
    class MoveAdapter extends RecyclerView.Adapter<MoveAdapter.MyViewHolder>{
        private ArrayList<Movimientos>moves;
        private Context context;

        public MoveAdapter(ArrayList<Movimientos> moves, Context cont) {
            this.moves = moves;
            this.context = cont;
        }

        @NonNull
        @Override
        public MoveAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.move_row, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MoveAdapter.MyViewHolder holder, int position) {
            Movimientos movimientos = moves.get(holder.getAdapterPosition());
            try {

                holder.movname.setText(movimientos.name.substring(0,1).toUpperCase()+movimientos.name.substring(1));
                if(movimientos.methodLearnName.equals("machine")){
                    holder.movMethod.setImageResource(R.drawable.mt);
                    holder.movLvl.setVisibility(View.GONE);
                }else if(movimientos.methodLearnName.equals("tutor")){
                    holder.movMethod.setImageResource(R.drawable.tutor);
                    holder.movLvl.setVisibility(View.GONE);
                }else if (movimientos.methodLearnName.equals("level-up")) {
                    holder.movLvl.setText("Level " + movimientos.levelLearnt);
                    holder.movMethod.setImageResource(R.drawable.candy);
                }

            }catch (Exception e){
            }

        }

        @Override
        public int getItemCount() {
            return moves.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            final TextView movname;
            final ImageView movMethod;
            final TextView movLvl;
            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                movname = itemView.findViewById(R.id.moveName);
                movMethod = itemView.findViewById(R.id.tutor);
                movLvl = itemView.findViewById(R.id.lvlLearne);
            }
        }
    }



    private void getPokeInfo() {
        for (Flavor_text_entries text : sp1.flavor_text_entries) {
            if (text.language.name.equals("es")&&!text.flavor_text.equals(null)) {
                String infogud = text.flavor_text.replaceAll("\n", " ");
                pokeInfo = pokeInfo + "\n"+" \n" + infogud;
            }

        }
        pokeInfo = pokeInfo.substring(pokeInfo.indexOf("\n")+1);
        infoPokemon.setText(pokeInfo);
    }







    private void getEvolutionChain(){
        Volley.newRequestQueue(this).add(new JsonObjectRequest(
                Request.Method.GET,
                evoLink,
                null,
                response -> {
                    JSONObject chain = response;
                    evolutionChain = new GsonBuilder().create().fromJson(chain.toString(), EvolutionChain.class);
                    if(evolutionChain.chain.evolves_to.isEmpty()){
                        evochan.setVisibility(View.INVISIBLE);
                        firstPokeChain.setVisibility(View.GONE);
                        nameFirstChain.setVisibility(View.GONE);
                        secondPokeChain8.setVisibility(View.GONE);
                        secondPokeChain7.setVisibility(View.GONE);
                        secondPokeChain6.setVisibility(View.GONE);
                        secondPokeChain5.setVisibility(View.GONE);
                        secondPokeChain4.setVisibility(View.GONE);
                        secondPokeChain3.setVisibility(View.GONE);
                        secondPokeChain2.setVisibility(View.GONE);
                        secondPokeChain.setVisibility(View.GONE);
                        secondPokeName.setVisibility(View.GONE);
                        secondPokeName2.setVisibility(View.GONE);
                        secondPokeName3.setVisibility(View.GONE);
                        secondPokeName4.setVisibility(View.GONE);
                        secondPokeName5.setVisibility(View.GONE);
                        secondPokeName6.setVisibility(View.GONE);
                        secondPokeName7.setVisibility(View.GONE);
                        secondPokeName8.setVisibility(View.GONE);
                        trigger.setVisibility(View.GONE);
                        trigger2.setVisibility(View.GONE);
                        trigger3.setVisibility(View.GONE);
                        trigger4.setVisibility(View.GONE);
                        trigger5.setVisibility(View.GONE);
                        trigger6.setVisibility(View.GONE);
                        trigger7.setVisibility(View.GONE);
                        trigger8.setVisibility(View.GONE);
                        condicion1rstEvo.setVisibility(View.GONE);
                        condicion1rstEvo1.setVisibility(View.GONE);
                        condicion1rstEvo2.setVisibility(View.GONE);
                        condicion1rstEvo3.setVisibility(View.GONE);
                        condicion1rstEvo4.setVisibility(View.GONE);
                        condicion1rstEvo5.setVisibility(View.GONE);
                        condicion1rstEvo6.setVisibility(View.GONE);
                        condicion1rstEvo7.setVisibility(View.GONE);
                        condicion2ndaEvo.setVisibility(View.GONE);
                        condicion2ndaEvo1.setVisibility(View.GONE);
                        condicion2ndaEvo2.setVisibility(View.GONE);
                        condicion2ndaEvo3.setVisibility(View.GONE);
                        condicion2ndaEvo4.setVisibility(View.GONE);
                        condicion2ndaEvo5.setVisibility(View.GONE);
                        condicion2ndaEvo6.setVisibility(View.GONE);
                        condicion2ndaEvo7.setVisibility(View.GONE);
                        thirdPokeChainAlt.setVisibility(View.GONE);
                        thirdPokeChain.setVisibility(View.GONE);
                        thirdPokeNameAlt.setVisibility(View.GONE);
                        thirsPokeName.setVisibility(View.GONE);
                        trigger9.setVisibility(View.GONE);
                        trigger1.setVisibility(View.GONE);
                        condicion1rstEvo8.setVisibility(View.GONE);
                        condicion1rstEvo9.setVisibility(View.GONE);
                        condicion2ndaEvo8.setVisibility(View.GONE);
                        condicion2ndaEvo9.setVisibility(View.GONE);
                    }


                    if(evolutionChain.chain.evolves_to.size()<7&&evolutionChain.chain.evolves_to.size()>=3){
                        secondPokeChain8.setVisibility(View.GONE);
                        secondPokeChain7.setVisibility(View.GONE);
                        secondPokeChain6.setVisibility(View.GONE);
                        secondPokeChain5.setVisibility(View.GONE);
                        secondPokeChain4.setVisibility(View.GONE);
                        secondPokeName4.setVisibility(View.GONE);
                        secondPokeName5.setVisibility(View.GONE);
                        secondPokeName6.setVisibility(View.GONE);
                        secondPokeName7.setVisibility(View.GONE);
                        secondPokeName8.setVisibility(View.GONE);
                        trigger4.setVisibility(View.GONE);
                        trigger5.setVisibility(View.GONE);
                        trigger6.setVisibility(View.GONE);
                        trigger7.setVisibility(View.GONE);
                        trigger8.setVisibility(View.GONE);
                        condicion1rstEvo3.setVisibility(View.GONE);
                        condicion1rstEvo4.setVisibility(View.GONE);
                        condicion1rstEvo5.setVisibility(View.GONE);
                        condicion1rstEvo6.setVisibility(View.GONE);
                        condicion1rstEvo7.setVisibility(View.GONE);
                        condicion2ndaEvo3.setVisibility(View.GONE);
                        condicion2ndaEvo4.setVisibility(View.GONE);
                        condicion2ndaEvo5.setVisibility(View.GONE);
                        condicion2ndaEvo6.setVisibility(View.GONE);
                        condicion2ndaEvo7.setVisibility(View.GONE);




                    }else if(evolutionChain.chain.evolves_to.size()==2){

                        secondPokeChain8.setVisibility(View.GONE);
                        secondPokeChain7.setVisibility(View.GONE);
                        secondPokeChain6.setVisibility(View.GONE);
                        secondPokeChain5.setVisibility(View.GONE);
                        secondPokeChain4.setVisibility(View.GONE);
                        secondPokeChain3.setVisibility(View.GONE);
                        secondPokeName3.setVisibility(View.GONE);
                        secondPokeName4.setVisibility(View.GONE);
                        secondPokeName5.setVisibility(View.GONE);
                        secondPokeName6.setVisibility(View.GONE);
                        secondPokeName7.setVisibility(View.GONE);
                        secondPokeName8.setVisibility(View.GONE);
                        trigger3.setVisibility(View.GONE);
                        trigger4.setVisibility(View.GONE);
                        trigger5.setVisibility(View.GONE);
                        trigger6.setVisibility(View.GONE);
                        trigger7.setVisibility(View.GONE);
                        trigger8.setVisibility(View.GONE);
                        condicion1rstEvo2.setVisibility(View.GONE);
                        condicion1rstEvo3.setVisibility(View.GONE);
                        condicion1rstEvo4.setVisibility(View.GONE);
                        condicion1rstEvo5.setVisibility(View.GONE);
                        condicion1rstEvo6.setVisibility(View.GONE);
                        condicion1rstEvo7.setVisibility(View.GONE);
                        condicion2ndaEvo2.setVisibility(View.GONE);
                        condicion2ndaEvo3.setVisibility(View.GONE);
                        condicion2ndaEvo4.setVisibility(View.GONE);
                        condicion2ndaEvo5.setVisibility(View.GONE);
                        condicion2ndaEvo6.setVisibility(View.GONE);
                        condicion2ndaEvo7.setVisibility(View.GONE);
                    }else if(evolutionChain.chain.evolves_to.size()==1){
                        secondPokeChain8.setVisibility(View.GONE);
                        secondPokeChain7.setVisibility(View.GONE);
                        secondPokeChain6.setVisibility(View.GONE);
                        secondPokeChain5.setVisibility(View.GONE);
                        secondPokeChain4.setVisibility(View.GONE);
                        secondPokeChain3.setVisibility(View.GONE);
                        secondPokeChain2.setVisibility(View.GONE);
                        secondPokeName2.setVisibility(View.GONE);
                        secondPokeName3.setVisibility(View.GONE);
                        secondPokeName4.setVisibility(View.GONE);
                        secondPokeName5.setVisibility(View.GONE);
                        secondPokeName6.setVisibility(View.GONE);
                        secondPokeName7.setVisibility(View.GONE);
                        secondPokeName8.setVisibility(View.GONE);
                        trigger2.setVisibility(View.GONE);
                        trigger3.setVisibility(View.GONE);
                        trigger4.setVisibility(View.GONE);
                        trigger5.setVisibility(View.GONE);
                        trigger6.setVisibility(View.GONE);
                        trigger7.setVisibility(View.GONE);
                        trigger8.setVisibility(View.GONE);
                        condicion1rstEvo1.setVisibility(View.GONE);
                        condicion1rstEvo2.setVisibility(View.GONE);
                        condicion1rstEvo3.setVisibility(View.GONE);
                        condicion1rstEvo4.setVisibility(View.GONE);
                        condicion1rstEvo5.setVisibility(View.GONE);
                        condicion1rstEvo6.setVisibility(View.GONE);
                        condicion1rstEvo7.setVisibility(View.GONE);
                        condicion2ndaEvo1.setVisibility(View.GONE);
                        condicion2ndaEvo2.setVisibility(View.GONE);
                        condicion2ndaEvo3.setVisibility(View.GONE);
                        condicion2ndaEvo4.setVisibility(View.GONE);
                        condicion2ndaEvo5.setVisibility(View.GONE);
                        condicion2ndaEvo6.setVisibility(View.GONE);
                        condicion2ndaEvo7.setVisibility(View.GONE);
                    }else if(evolutionChain.chain.evolves_to.size()==0){

                        //evochan.setVisibility(View.INVISIBLE);
                        secondPokeChain8.setVisibility(View.GONE);
                        secondPokeChain7.setVisibility(View.GONE);
                        secondPokeChain6.setVisibility(View.GONE);
                        secondPokeChain5.setVisibility(View.GONE);
                        secondPokeChain4.setVisibility(View.GONE);
                        secondPokeChain3.setVisibility(View.GONE);
                        secondPokeChain2.setVisibility(View.GONE);
                        secondPokeChain.setVisibility(View.GONE);
                        secondPokeName.setVisibility(View.GONE);
                        secondPokeName2.setVisibility(View.GONE);
                        secondPokeName3.setVisibility(View.GONE);
                        secondPokeName4.setVisibility(View.GONE);
                        secondPokeName5.setVisibility(View.GONE);
                        secondPokeName6.setVisibility(View.GONE);
                        secondPokeName7.setVisibility(View.GONE);
                        secondPokeName8.setVisibility(View.GONE);
                        trigger.setVisibility(View.GONE);
                        trigger2.setVisibility(View.GONE);
                        trigger3.setVisibility(View.GONE);
                        trigger4.setVisibility(View.GONE);
                        trigger5.setVisibility(View.GONE);
                        trigger6.setVisibility(View.GONE);
                        trigger7.setVisibility(View.GONE);
                        trigger8.setVisibility(View.GONE);
                        condicion1rstEvo.setVisibility(View.GONE);
                        condicion1rstEvo1.setVisibility(View.GONE);
                        condicion1rstEvo2.setVisibility(View.GONE);
                        condicion1rstEvo3.setVisibility(View.GONE);
                        condicion1rstEvo4.setVisibility(View.GONE);
                        condicion1rstEvo5.setVisibility(View.GONE);
                        condicion1rstEvo6.setVisibility(View.GONE);
                        condicion1rstEvo7.setVisibility(View.GONE);
                        condicion2ndaEvo.setVisibility(View.GONE);
                        condicion2ndaEvo1.setVisibility(View.GONE);
                        condicion2ndaEvo2.setVisibility(View.GONE);
                        condicion2ndaEvo3.setVisibility(View.GONE);
                        condicion2ndaEvo4.setVisibility(View.GONE);
                        condicion2ndaEvo5.setVisibility(View.GONE);
                        condicion2ndaEvo6.setVisibility(View.GONE);
                        condicion2ndaEvo7.setVisibility(View.GONE);
                    }





                    int counter = 0;
                    for(Evolves_to evolves_toes:evolutionChain.chain.evolves_to){
                        try {
                            if (counter == 0) {
                                url2ndPoke = evolves_toes.species.url;
                                for (Evolution_Details details : evolves_toes.evolution_details) {
                                    triggero1 = details.trigger.name;
                                    if (details.held_item != null && cond1 == null) {
                                        cond1 = details.held_item.name;
                                    } else if (details.held_item != null && cond1 != null) {
                                        cond2 = details.held_item.name;
                                    }
                                    if (details.gender == 1 && cond1 == null) {
                                        cond1 = "Female";
                                    } else if (details.gender == 1 && cond1 != null) {
                                        cond2 = "Female";
                                    }
                                    if (details.item != null && cond1 == null) {
                                        cond1 = details.item.name;
                                    } else if (details.item != null && cond1 != null) {
                                        cond2 = details.item.name;
                                    }
                                    if (details.known_move != null && cond1 == null) {
                                        cond1 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond1 != null) {
                                        cond2 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.known_move_type != null && cond1 == null) {
                                        cond1 = "Knowing " + details.known_move_type.name + " move";
                                    } else if (details.known_move != null && cond1 != null) {
                                        cond2 = "Knowing " + details.known_move_type.name + " move";
                                    }
                                    if (details.location != null && cond1 == null) {
                                        cond1 = "In " + details.location.name;
                                    } else if (details.location != null && cond1 != null) {
                                        cond2 = "In " + details.location.name;
                                    }
                                    if (details.min_happiness > 0 && cond1 == null) {
                                        cond1 = "Min. Happiness " + details.min_happiness;
                                    } else if (details.min_happiness > 0 && cond1 != null) {
                                        cond2 = "Min. Happiness " + details.min_happiness;
                                    }
                                    if (details.min_beauty > 0 && cond1 == null) {
                                        cond1 = "Min. Beauty " + details.min_beauty;
                                    } else if (details.min_beauty > 0 && cond1 != null) {
                                        cond2 = "Min.Beauty " + details.min_beauty;
                                    }
                                    if (details.min_level > 0 && cond1 == null) {
                                        cond1 = "Lvl " + details.min_level;
                                    } else if (details.min_level > 0 && cond1 != null) {
                                        cond2 = "Lvl " + details.min_level;
                                    }
                                    if (details.known_move != null && cond1 == null) {
                                        cond1 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond1 != null) {
                                        cond2 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.needs_overworld_rain != false && cond1 == null) {
                                        cond1 = "Needs Overworld Rain";
                                    } else if (details.needs_overworld_rain != false && cond1 != null) {
                                        cond2 = "Needs Overworld Rain";
                                    }
                                    if (details.party_species != null && cond1 == null) {
                                        cond1 = "With " + details.party_species.name + " in the team";
                                    } else if (details.party_species != null && cond1 != null) {
                                        cond2 = "With " + details.party_species.name + "in the team";
                                    }
                                    if (details.party_type != null && cond1 == null) {
                                        cond1 = "With a type " + details.party_type.name + " in the team";
                                    } else if (details.party_type != null && cond1 != null) {
                                        cond2 = "With a type " + details.party_type.name + " in the team";
                                    }
                                    if (details.relative_physical_stats == 1 && cond1 == null) {
                                        cond1 = "With more Attack than Defense";
                                    } else if (details.relative_physical_stats == 1 && cond1 != null) {
                                        cond2 = "With more Attack than Defense";
                                    }
                                    if (details.relative_physical_stats == -1 && cond1 == null) {
                                        cond1 = "With more Defense than Attack";
                                    } else if (details.relative_physical_stats == -1 && cond1 != null) {
                                        cond2 = "With more Defense than Attack";
                                    }
                                    if (details.relative_physical_stats == 0 && cond1 == null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond1 = "With even Attack and Defense";
                                    } else if (details.relative_physical_stats == 0 && cond1 != null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond2 = "With even Attack and Defense";
                                    }
                                    if (details.time_of_day.equals("day") && cond1 == null) {
                                        cond1 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("day") && cond1 != null) {
                                        cond2 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("dusk") && cond1 == null) {
                                        cond1 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("dusk") && cond1 != null) {
                                        cond2 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("night") && cond1 == null) {
                                        cond1 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("night") && cond1 != null) {
                                        cond2 = "At " + details.time_of_day;
                                    }
                                    if (details.trade_species != null && cond1 == null) {
                                        cond1 = "With " + details.trade_species.name;
                                    } else if (details.trade_species != null && cond1 != null) {
                                        cond2 = "With " + details.trade_species.name;
                                    }
                                    if (details.turn_uspide_down != false && cond1 == null) {
                                        cond1 = "Turning console Upside Down";
                                    } else if (details.turn_uspide_down != false && cond1 != null) {
                                        cond2 = "Turning console Upside Down";
                                    }

                                }
                            } else if (counter == 1) {
                                url2ndPokeAlt = evolves_toes.species.url;
                                for (Evolution_Details details : evolves_toes.evolution_details) {
                                    triggero2 = details.trigger.name;
                                    if (details.held_item != null && cond3 == null) {
                                        cond3 = details.held_item.name;
                                    } else if (details.held_item != null && cond3 != null) {
                                        cond4 = details.held_item.name;
                                    }
                                    if (details.gender == 1 && cond3 == null) {
                                        cond3 = "Female";
                                    } else if (details.gender == 1 && cond3 != null) {
                                        cond4 = "Female";
                                    }
                                    if (details.item != null && cond3 == null) {
                                        cond3 = details.item.name;
                                    } else if (details.item != null && cond3 != null) {
                                        cond4 = details.item.name;
                                    }
                                    if (details.known_move != null && cond3 == null) {
                                        cond3 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond3 != null) {
                                        cond4 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.known_move_type != null && cond3 == null) {
                                        cond3 = "Knowing " + details.known_move_type.name + " move";
                                    } else if (details.known_move != null && cond3 != null) {
                                        cond4 = "Knowing " + details.known_move_type.name + " move";
                                    }
                                    if (details.location != null && cond3 == null) {
                                        cond3 = "In " + details.location.name;
                                    } else if (details.location != null && cond3 != null) {
                                        cond4 = "In " + details.location.name;
                                    }
                                    if (details.min_happiness > 0 && cond3 == null) {
                                        cond3 = "Min. Happiness " + details.min_happiness;
                                    } else if (details.min_happiness > 0 && cond3 != null) {
                                        cond4 = "Min. Happiness " + details.min_happiness;
                                    }
                                    if (details.min_beauty > 0 && cond3 == null) {
                                        cond3 = "Min. Beauty " + details.min_beauty;
                                    } else if (details.min_beauty > 0 && cond3 != null) {
                                        cond4 = "Min.Beauty " + details.min_beauty;
                                    }
                                    if (details.min_level > 0 && cond3 == null) {
                                        cond3 = "Lvl " + details.min_level;
                                    } else if (details.min_level > 0 && cond3 != null) {
                                        cond4 = "Lvl " + details.min_level;
                                    }
                                    if (details.known_move != null && cond3 == null) {
                                        cond3 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond3 != null) {
                                        cond4 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.needs_overworld_rain != false && cond3 == null) {
                                        cond3 = "Needs Overworld Rain";
                                    } else if (details.needs_overworld_rain != false && cond3 != null) {
                                        cond4 = "Needs Overworld Rain";
                                    }
                                    if (details.party_species != null && cond3 == null) {
                                        cond3 = "With " + details.party_species.name + " in the team";
                                    } else if (details.party_species != null && cond3 != null) {
                                        cond4 = "With " + details.party_species.name + "in the team";
                                    }
                                    if (details.party_type != null && cond3 == null) {
                                        cond3 = "With a type " + details.party_type.name + " in the team";
                                    } else if (details.party_type != null && cond3 != null) {
                                        cond4 = "With a type " + details.party_type.name + " in the team";
                                    }
                                    if (details.relative_physical_stats == 1 && cond3 == null) {
                                        cond3 = "With more Attack than Defense";
                                    } else if (details.relative_physical_stats == 1 && cond3 != null) {
                                        cond4 = "With more Attack than Defense";
                                    }
                                    if (details.relative_physical_stats == -1 && cond3 == null) {
                                        cond3 = "With more Defense than Attack";
                                    } else if (details.relative_physical_stats == -1 && cond3 != null) {
                                        cond4 = "With more Defense than Attack";
                                    }
                                    if (details.relative_physical_stats == 0 && cond3 == null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond3 = "With even Attack and Defense";
                                    } else if (details.relative_physical_stats == 0 && cond3 != null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond4 = "With even Attack and Defense";
                                    }
                                    if (details.time_of_day.equals("day") && cond3 == null) {
                                        cond3 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("day") && cond3 != null) {
                                        cond4 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("dusk") && cond3 == null) {
                                        cond3 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("dusk") && cond3 != null) {
                                        cond4 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("night") && cond3 == null) {
                                        cond3 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("night") && cond3 != null) {
                                        cond4 = "At " + details.time_of_day;
                                    }
                                    if (details.trade_species != null && cond3 == null) {
                                        cond3 = "With " + details.trade_species.name;
                                    } else if (details.trade_species != null && cond3 != null) {
                                        cond4 = "With " + details.trade_species.name;
                                    }
                                    if (details.turn_uspide_down != false && cond3 == null) {
                                        cond3 = "Turning console Upside Down";
                                    } else if (details.turn_uspide_down != false && cond3 != null) {
                                        cond4 = "Turning console Upside Down";
                                    }
                                }
                            } else if (counter == 2) {
                                url2ndPokeAlt1 = evolves_toes.species.url;
                                for (Evolution_Details details : evolves_toes.evolution_details) {
                                    triggero3 = details.trigger.name;
                                    if (details.held_item != null && cond5 == null) {
                                        cond5 = details.held_item.name;
                                    } else if (details.held_item != null && cond5 != null) {
                                        cond6 = details.held_item.name;
                                    }
                                    if (details.gender == 1 && cond5 == null) {
                                        cond5 = "Female";
                                    } else if (details.gender == 1 && cond5 != null) {
                                        cond6 = "Female";
                                    }
                                    if (details.item != null && cond5 == null) {
                                        cond5 = details.item.name;
                                    } else if (details.item != null && cond5 != null) {
                                        cond6 = details.item.name;
                                    }
                                    if (details.known_move != null && cond5 == null) {
                                        cond5 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond5 != null) {
                                        cond6 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.known_move_type != null && cond5 == null) {
                                        cond5 = "Knowing " + details.known_move_type.name + " move";
                                    } else if (details.known_move != null && cond5 != null) {
                                        cond6 = "Knowing " + details.known_move_type.name + " move";
                                    }
                                    if (details.location != null && cond5 == null) {
                                        cond5 = "In " + details.location.name;
                                    } else if (details.location != null && cond5 != null) {
                                        cond6 = "In " + details.location.name;
                                    }
                                    if (details.min_happiness > 0 && cond5 == null) {
                                        cond5 = "Min. Happiness " + details.min_happiness;
                                    } else if (details.min_happiness > 0 && cond5 != null) {
                                        cond6 = "Min. Happiness " + details.min_happiness;
                                    }
                                    if (details.min_beauty > 0 && cond5 == null) {
                                        cond5 = "Min. Beauty " + details.min_beauty;
                                    } else if (details.min_beauty > 0 && cond5 != null) {
                                        cond6 = "Min.Beauty " + details.min_beauty;
                                    }
                                    if (details.min_level > 0 && cond5 == null) {
                                        cond5 = "Lvl " + details.min_level;
                                    } else if (details.min_level > 0 && cond5 != null) {
                                        cond6 = "Lvl " + details.min_level;
                                    }
                                    if (details.known_move != null && cond5 == null) {
                                        cond5 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond5 != null) {
                                        cond6 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.needs_overworld_rain != false && cond5 == null) {
                                        cond5 = "Needs Overworld Rain";
                                    } else if (details.needs_overworld_rain != false && cond5 != null) {
                                        cond6 = "Needs Overworld Rain";
                                    }
                                    if (details.party_species != null && cond5 == null) {
                                        cond5 = "With " + details.party_species.name + " in the team";
                                    } else if (details.party_species != null && cond5 != null) {
                                        cond6 = "With " + details.party_species.name + "in the team";
                                    }
                                    if (details.party_type != null && cond5 == null) {
                                        cond5 = "With a type " + details.party_type.name + " in the team";
                                    } else if (details.party_type != null && cond5 != null) {
                                        cond6 = "With a type " + details.party_type.name + " in the team";
                                    }
                                    if (details.relative_physical_stats == 1 && cond5 == null) {
                                        cond5 = "With more Attack than Defense";
                                    } else if (details.relative_physical_stats == 1 && cond5 != null) {
                                        cond6 = "With more Attack than Defense";
                                    }
                                    if (details.relative_physical_stats == -1 && cond5 == null) {
                                        cond5 = "With more Defense than Attack";
                                    } else if (details.relative_physical_stats == -1 && cond5 != null) {
                                        cond6 = "With more Defense than Attack";
                                    }
                                    if (details.relative_physical_stats == 0 && cond5 == null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond5 = "With even Attack and Defense";
                                    } else if (details.relative_physical_stats == 0 && cond5 != null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond6 = "With even Attack and Defense";
                                    }
                                    if (details.time_of_day.equals("day") && cond5 == null) {
                                        cond5 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("day") && cond5 != null) {
                                        cond6 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("dusk") && cond5 == null) {
                                        cond5 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("dusk") && cond5 != null) {
                                        cond6 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("night") && cond5 == null) {
                                        cond5 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("night") && cond5 != null) {
                                        cond6 = "At " + details.time_of_day;
                                    }
                                    if (details.trade_species != null && cond5 == null) {
                                        cond5 = "With " + details.trade_species.name;
                                    } else if (details.trade_species != null && cond5 != null) {
                                        cond6 = "With " + details.trade_species.name;
                                    }
                                    if (details.turn_uspide_down != false && cond5 == null) {
                                        cond5 = "Turning console Upside Down";
                                    } else if (details.turn_uspide_down != false && cond5 != null) {
                                        cond6 = "Turning console Upside Down";
                                    }
                                }
                            } else if (counter == 3) {
                                url2ndPokeAlt2 = evolves_toes.species.url;
                                for (Evolution_Details details : evolves_toes.evolution_details) {
                                    triggero4 = details.trigger.name;
                                    if (details.held_item != null && cond7 == null) {
                                        cond7 = details.held_item.name;
                                    } else if (details.held_item != null && cond7 != null) {
                                        cond8 = details.held_item.name;
                                    }
                                    if (details.gender == 1 && cond7 == null) {
                                        cond7 = "Female";
                                    } else if (details.gender == 1 && cond7 != null) {
                                        cond8 = "Female";
                                    }
                                    if (details.item != null && cond7 == null) {
                                        cond7 = details.item.name;
                                    } else if (details.item != null && cond7 != null) {
                                        cond8 = details.item.name;
                                    }
                                    if (details.known_move != null && cond7 == null) {
                                        cond7 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond7 != null) {
                                        cond8 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.known_move_type != null && cond7 == null) {
                                        cond7 = "Knowing " + details.known_move_type.name + " move";
                                    } else if (details.known_move != null && cond7 != null) {
                                        cond8 = "Knowing " + details.known_move_type.name + " move";
                                    }
                                    if (details.location != null && cond7 == null) {
                                        cond7 = "In " + details.location.name;
                                    } else if (details.location != null && cond7 != null) {
                                        cond8 = "In " + details.location.name;
                                    }
                                    if (details.min_happiness > 0 && cond7 == null) {
                                        cond7 = "Min. Happiness " + details.min_happiness;
                                    } else if (details.min_happiness > 0 && cond7 != null) {
                                        cond8 = "Min. Happiness " + details.min_happiness;
                                    }
                                    if (details.min_beauty > 0 && cond7 == null) {
                                        cond7 = "Min. Beauty " + details.min_beauty;
                                    } else if (details.min_beauty > 0 && cond7 != null) {
                                        cond8 = "Min.Beauty " + details.min_beauty;
                                    }
                                    if (details.min_level > 0 && cond7 == null) {
                                        cond7 = "Lvl " + details.min_level;
                                    } else if (details.min_level > 0 && cond7 != null) {
                                        cond8 = "Lvl " + details.min_level;
                                    }
                                    if (details.known_move != null && cond7 == null) {
                                        cond7 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond7 != null) {
                                        cond8 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.needs_overworld_rain != false && cond7 == null) {
                                        cond7 = "Needs Overworld Rain";
                                    } else if (details.needs_overworld_rain != false && cond7 != null) {
                                        cond8 = "Needs Overworld Rain";
                                    }
                                    if (details.party_species != null && cond7 == null) {
                                        cond7 = "With " + details.party_species.name + " in the team";
                                    } else if (details.party_species != null && cond7 != null) {
                                        cond8 = "With " + details.party_species.name + "in the team";
                                    }
                                    if (details.party_type != null && cond7 == null) {
                                        cond7 = "With a type " + details.party_type.name + " in the team";
                                    } else if (details.party_type != null && cond7 != null) {
                                        cond8 = "With a type " + details.party_type.name + " in the team";
                                    }
                                    if (details.relative_physical_stats == 1 && cond7 == null) {
                                        cond7 = "With more Attack than Defense";
                                    } else if (details.relative_physical_stats == 1 && cond7 != null) {
                                        cond8 = "With more Attack than Defense";
                                    }
                                    if (details.relative_physical_stats == -1 && cond7 == null) {
                                        cond7 = "With more Defense than Attack";
                                    } else if (details.relative_physical_stats == -1 && cond7 != null) {
                                        cond8 = "With more Defense than Attack";
                                    }
                                    if (details.relative_physical_stats == 0 && cond7 == null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond7 = "With even Attack and Defense";
                                    } else if (details.relative_physical_stats == 0 && cond7 != null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond8 = "With even Attack and Defense";
                                    }
                                    if (details.time_of_day.equals("day") && cond7 == null) {
                                        cond7 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("day") && cond7 != null) {
                                        cond8 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("dusk") && cond7 == null) {
                                        cond7 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("dusk") && cond7 != null) {
                                        cond8 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("night") && cond7 == null) {
                                        cond7 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("night") && cond7 != null) {
                                        cond8 = "At " + details.time_of_day;
                                    }
                                    if (details.trade_species != null && cond7 == null) {
                                        cond7 = "With " + details.trade_species.name;
                                    } else if (details.trade_species != null && cond7 != null) {
                                        cond8 = "With " + details.trade_species.name;
                                    }
                                    if (details.turn_uspide_down != false && cond7 == null) {
                                        cond7 = "Turning console Upside Down";
                                    } else if (details.turn_uspide_down != false && cond7 != null) {
                                        cond8 = "Turning console Upside Down";
                                    }
                                }
                            } else if (counter == 4) {
                                url2ndPokeAlt3 = evolves_toes.species.url;
                                for (Evolution_Details details : evolves_toes.evolution_details) {
                                    triggero5 = details.trigger.name;
                                    if (details.held_item != null && cond9 == null) {
                                        cond9 = details.held_item.name;
                                    } else if (details.held_item != null && cond9 != null) {
                                        cond10 = details.held_item.name;
                                    }
                                    if (details.gender == 1 && cond9 == null) {
                                        cond9 = "Female";
                                    } else if (details.gender == 1 && cond9 != null) {
                                        cond10 = "Female";
                                    }
                                    if (details.item != null && cond9 == null) {
                                        cond9 = details.item.name;
                                    } else if (details.item != null && cond9 != null) {
                                        cond10 = details.item.name;
                                    }
                                    if (details.known_move != null && cond9 == null) {
                                        cond9 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond9 != null) {
                                        cond10 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.known_move_type != null && cond9 == null) {
                                        cond9 = "Knowing " + details.known_move_type.name + " move";
                                    } else if (details.known_move != null && cond9 != null) {
                                        cond10 = "Knowing " + details.known_move_type.name + " move";
                                    }
                                    if (details.location != null && cond9 == null) {
                                        cond9 = "In " + details.location.name;
                                    } else if (details.location != null && cond9 != null) {
                                        cond10 = "In " + details.location.name;
                                    }
                                    if (details.min_happiness > 0 && cond9 == null) {
                                        cond9 = "Min. Happiness " + details.min_happiness;
                                    } else if (details.min_happiness > 0 && cond9 != null) {
                                        cond10 = "Min. Happiness " + details.min_happiness;
                                    }
                                    if (details.min_beauty > 0 && cond9 == null) {
                                        cond9 = "Min. Beauty " + details.min_beauty;
                                    } else if (details.min_beauty > 0 && cond9 != null) {
                                        cond10 = "Min.Beauty " + details.min_beauty;
                                    }
                                    if (details.min_level > 0 && cond9 == null) {
                                        cond9 = "Lvl " + details.min_level;
                                    } else if (details.min_level > 0 && cond9 != null) {
                                        cond10 = "Lvl " + details.min_level;
                                    }
                                    if (details.known_move != null && cond9 == null) {
                                        cond9 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond9 != null) {
                                        cond10 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.needs_overworld_rain != false && cond9 == null) {
                                        cond9 = "Needs Overworld Rain";
                                    } else if (details.needs_overworld_rain != false && cond9 != null) {
                                        cond10 = "Needs Overworld Rain";
                                    }
                                    if (details.party_species != null && cond9 == null) {
                                        cond9 = "With " + details.party_species.name + " in the team";
                                    } else if (details.party_species != null && cond9 != null) {
                                        cond10 = "With " + details.party_species.name + "in the team";
                                    }
                                    if (details.party_type != null && cond9 == null) {
                                        cond9 = "With a type " + details.party_type.name + " in the team";
                                    } else if (details.party_type != null && cond9 != null) {
                                        cond10 = "With a type " + details.party_type.name + " in the team";
                                    }
                                    if (details.relative_physical_stats == 1 && cond9 == null) {
                                        cond9 = "With more Attack than Defense";
                                    } else if (details.relative_physical_stats == 1 && cond9 != null) {
                                        cond10 = "With more Attack than Defense";
                                    }
                                    if (details.relative_physical_stats == -1 && cond9 == null) {
                                        cond9 = "With more Defense than Attack";
                                    } else if (details.relative_physical_stats == -1 && cond9 != null) {
                                        cond10 = "With more Defense than Attack";
                                    }
                                    if (details.relative_physical_stats == 0 && cond9 == null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond9 = "With even Attack and Defense";
                                    } else if (details.relative_physical_stats == 0 && cond9 != null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond10 = "With even Attack and Defense";
                                    }
                                    if (details.time_of_day.equals("day") && cond9 == null) {
                                        cond9 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("day") && cond9 != null) {
                                        cond10 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("dusk") && cond9 == null) {
                                        cond9 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("dusk") && cond9 != null) {
                                        cond10 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("night") && cond9 == null) {
                                        cond9 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("night") && cond9 != null) {
                                        cond10 = "At " + details.time_of_day;
                                    }
                                    if (details.trade_species != null && cond9 == null) {
                                        cond9 = "With " + details.trade_species.name;
                                    } else if (details.trade_species != null && cond9 != null) {
                                        cond10 = "With " + details.trade_species.name;
                                    }
                                    if (details.turn_uspide_down != false && cond9 == null) {
                                        cond9 = "Turning console Upside Down";
                                    } else if (details.turn_uspide_down != false && cond9 != null) {
                                        cond10 = "Turning console Upside Down";
                                    }
                                }
                            } else if (counter == 5) {
                                url2ndPokeAlt4 = evolves_toes.species.url;
                                for (Evolution_Details details : evolves_toes.evolution_details) {
                                    triggero6 = details.trigger.name;
                                    if (details.held_item != null && cond11 == null) {
                                        cond11 = details.held_item.name;
                                    } else if (details.held_item != null && cond11 != null) {
                                        cond12 = details.held_item.name;
                                    }
                                    if (details.gender == 1 && cond11 == null) {
                                        cond11 = "Female";
                                    } else if (details.gender == 1 && cond11 != null) {
                                        cond12 = "Female";
                                    }
                                    if (details.item != null && cond11 == null) {
                                        cond11 = details.item.name;
                                    } else if (details.item != null && cond11 != null) {
                                        cond12 = details.item.name;
                                    }
                                    if (details.known_move != null && cond11 == null) {
                                        cond11 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond11 != null) {
                                        cond12 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.known_move_type != null && cond11 == null) {
                                        cond11 = "Knowing " + details.known_move_type.name + " move";
                                    } else if (details.known_move != null && cond11 != null) {
                                        cond12 = "Knowing " + details.known_move_type.name + " move";
                                    }
                                    if (details.location != null && cond11 == null) {
                                        cond11 = "In " + details.location.name;
                                    } else if (details.location != null && cond11 != null) {
                                        cond12 = "In " + details.location.name;
                                    }
                                    if (details.min_happiness > 0 && cond11 == null) {
                                        cond11 = "Min. Happiness " + details.min_happiness;
                                    } else if (details.min_happiness > 0 && cond11 != null) {
                                        cond12 = "Min. Happiness " + details.min_happiness;
                                    }
                                    if (details.min_beauty > 0 && cond11 == null) {
                                        cond11 = "Min. Beauty " + details.min_beauty;
                                    } else if (details.min_beauty > 0 && cond11 != null) {
                                        cond12 = "Min.Beauty " + details.min_beauty;
                                    }
                                    if (details.min_level > 0 && cond11 == null) {
                                        cond11 = "Lvl " + details.min_level;
                                    } else if (details.min_level > 0 && cond11 != null) {
                                        cond12 = "Lvl " + details.min_level;
                                    }
                                    if (details.known_move != null && cond11 == null) {
                                        cond11 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond11 != null) {
                                        cond12 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.needs_overworld_rain != false && cond11 == null) {
                                        cond11 = "Needs Overworld Rain";
                                    } else if (details.needs_overworld_rain != false && cond11 != null) {
                                        cond12 = "Needs Overworld Rain";
                                    }
                                    if (details.party_species != null && cond11 == null) {
                                        cond11 = "With " + details.party_species.name + " in the team";
                                    } else if (details.party_species != null && cond11 != null) {
                                        cond12 = "With " + details.party_species.name + "in the team";
                                    }
                                    if (details.party_type != null && cond11 == null) {
                                        cond11 = "With a type " + details.party_type.name + " in the team";
                                    } else if (details.party_type != null && cond11 != null) {
                                        cond12 = "With a type " + details.party_type.name + " in the team";
                                    }
                                    if (details.relative_physical_stats == 1 && cond11 == null) {
                                        cond11 = "With more Attack than Defense";
                                    } else if (details.relative_physical_stats == 1 && cond11 != null) {
                                        cond12 = "With more Attack than Defense";
                                    }
                                    if (details.relative_physical_stats == -1 && cond11 == null) {
                                        cond11 = "With more Defense than Attack";
                                    } else if (details.relative_physical_stats == -1 && cond11 != null) {
                                        cond12 = "With more Defense than Attack";
                                    }
                                    if (details.relative_physical_stats == 0 && cond11 == null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond11 = "With even Attack and Defense";
                                    } else if (details.relative_physical_stats == 0 && cond11 != null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond12 = "With even Attack and Defense";
                                    }
                                    if (details.time_of_day.equals("day") && cond11 == null) {
                                        cond11 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("day") && cond11 != null) {
                                        cond12 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("dusk") && cond11 == null) {
                                        cond11 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("dusk") && cond11 != null) {
                                        cond12 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("night") && cond11 == null) {
                                        cond11 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("night") && cond11 != null) {
                                        cond12 = "At " + details.time_of_day;
                                    }
                                    if (details.trade_species != null && cond11 == null) {
                                        cond11 = "With " + details.trade_species.name;
                                    } else if (details.trade_species != null && cond11 != null) {
                                        cond12 = "With " + details.trade_species.name;
                                    }
                                    if (details.turn_uspide_down != false && cond11 == null) {
                                        cond11 = "Turning console Upside Down";
                                    } else if (details.turn_uspide_down != false && cond11 != null) {
                                        cond12 = "Turning console Upside Down";
                                    }
                                }
                            } else if (counter == 6) {
                                url2ndPokeAlt5 = evolves_toes.species.url;
                                for (Evolution_Details details : evolves_toes.evolution_details) {
                                    triggero7 = details.trigger.name;
                                    if (details.held_item != null && cond13 == null) {
                                        cond13 = details.held_item.name;
                                    } else if (details.held_item != null && cond13 != null) {
                                        cond14 = details.held_item.name;
                                    }
                                    if (details.gender == 1 && cond13 == null) {
                                        cond13 = "Female";
                                    } else if (details.gender == 1 && cond13 != null) {
                                        cond14 = "Female";
                                    }
                                    if (details.item != null && cond13 == null) {
                                        cond13 = details.item.name;
                                    } else if (details.item != null && cond13 != null) {
                                        cond14 = details.item.name;
                                    }
                                    if (details.known_move != null && cond13 == null) {
                                        cond13 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond13 != null) {
                                        cond14 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.known_move_type != null && cond13 == null) {
                                        cond13 = "Knowing " + details.known_move_type.name + " move";
                                    } else if (details.known_move != null && cond13 != null) {
                                        cond14 = "Knowing " + details.known_move_type.name + " move";
                                    }
                                    if (details.location != null && cond13 == null) {
                                        cond13 = "In " + details.location.name;
                                    } else if (details.location != null && cond13 != null) {
                                        cond14 = "In " + details.location.name;
                                    }
                                    if (details.min_happiness > 0 && cond13 == null) {
                                        cond13 = "Min. Happiness " + details.min_happiness;
                                    } else if (details.min_happiness > 0 && cond13 != null) {
                                        cond14 = "Min. Happiness " + details.min_happiness;
                                    }
                                    if (details.min_beauty > 0 && cond13 == null) {
                                        cond13 = "Min. Beauty " + details.min_beauty;
                                    } else if (details.min_beauty > 0 && cond13 != null) {
                                        cond14 = "Min.Beauty " + details.min_beauty;
                                    }
                                    if (details.min_level > 0 && cond13 == null) {
                                        cond13 = "Lvl " + details.min_level;
                                    } else if (details.min_level > 0 && cond13 != null) {
                                        cond14 = "Lvl " + details.min_level;
                                    }
                                    if (details.known_move != null && cond13 == null) {
                                        cond13 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond13 != null) {
                                        cond14 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.needs_overworld_rain != false && cond13 == null) {
                                        cond13 = "Needs Overworld Rain";
                                    } else if (details.needs_overworld_rain != false && cond13 != null) {
                                        cond14 = "Needs Overworld Rain";
                                    }
                                    if (details.party_species != null && cond13 == null) {
                                        cond13 = "With " + details.party_species.name + " in the team";
                                    } else if (details.party_species != null && cond13 != null) {
                                        cond14 = "With " + details.party_species.name + "in the team";
                                    }
                                    if (details.party_type != null && cond13 == null) {
                                        cond13 = "With a type " + details.party_type.name + " in the team";
                                    } else if (details.party_type != null && cond13 != null) {
                                        cond14 = "With a type " + details.party_type.name + " in the team";
                                    }
                                    if (details.relative_physical_stats == 1 && cond13 == null) {
                                        cond13 = "With more Attack than Defense";
                                    } else if (details.relative_physical_stats == 1 && cond13 != null) {
                                        cond14 = "With more Attack than Defense";
                                    }
                                    if (details.relative_physical_stats == -1 && cond13 == null) {
                                        cond13 = "With more Defense than Attack";
                                    } else if (details.relative_physical_stats == -1 && cond13 != null) {
                                        cond14 = "With more Defense than Attack";
                                    }
                                    if (details.relative_physical_stats == 0 && cond13 == null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond13 = "With even Attack and Defense";
                                    } else if (details.relative_physical_stats == 0 && cond13 != null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond14 = "With even Attack and Defense";
                                    }
                                    if (details.time_of_day.equals("day") && cond13 == null) {
                                        cond13 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("day") && cond13 != null) {
                                        cond14 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("dusk") && cond13 == null) {
                                        cond13 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("dusk") && cond13 != null) {
                                        cond14 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("night") && cond13 == null) {
                                        cond13 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("night") && cond13 != null) {
                                        cond14 = "At " + details.time_of_day;
                                    }
                                    if (details.trade_species != null && cond13 == null) {
                                        cond13 = "With " + details.trade_species.name;
                                    } else if (details.trade_species != null && cond13 != null) {
                                        cond14 = "With " + details.trade_species.name;
                                    }
                                    if (details.turn_uspide_down != false && cond13 == null) {
                                        cond13 = "Turning console Upside Down";
                                    } else if (details.turn_uspide_down != false && cond13 != null) {
                                        cond14 = "Turning console Upside Down";
                                    }
                                }
                            } else if (counter == 7) {
                                url2ndPokeAlt6 = evolves_toes.species.url;
                                for (Evolution_Details details : evolves_toes.evolution_details) {
                                    triggero8 = details.trigger.name;
                                    if (details.held_item != null && cond15 == null) {
                                        cond15 = details.held_item.name;
                                    } else if (details.held_item != null && cond15 != null) {
                                        cond16 = details.held_item.name;
                                    }
                                    if (details.gender == 1 && cond15 == null) {
                                        cond15 = "Female";
                                    } else if (details.gender == 1 && cond15 != null) {
                                        cond16 = "Female";
                                    }
                                    if (details.item != null && cond15 == null) {
                                        cond15 = details.item.name;
                                    } else if (details.item != null && cond15 != null) {
                                        cond16 = details.item.name;
                                    }
                                    if (details.known_move != null && cond15 == null) {
                                        cond15 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond15 != null) {
                                        cond16 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.known_move_type != null && cond15 == null) {
                                        cond15 = "Knowing " + details.known_move_type.name + " move";
                                    } else if (details.known_move != null && cond15 != null) {
                                        cond16 = "Knowing " + details.known_move_type.name + " move";
                                    }
                                    if (details.location != null && cond15 == null) {
                                        cond15 = "In " + details.location.name;
                                    } else if (details.location != null && cond15 != null) {
                                        cond16 = "In " + details.location.name;
                                    }
                                    if (details.min_happiness > 0 && cond15 == null) {
                                        cond15 = "Min. Happiness " + details.min_happiness;
                                    } else if (details.min_happiness > 0 && cond15 != null) {
                                        cond16 = "Min. Happiness " + details.min_happiness;
                                    }
                                    if (details.min_beauty > 0 && cond15 == null) {
                                        cond15 = "Min. Beauty " + details.min_beauty;
                                    } else if (details.min_beauty > 0 && cond15 != null) {
                                        cond16 = "Min.Beauty " + details.min_beauty;
                                    }
                                    if (details.min_level > 0 && cond15 == null) {
                                        cond15 = "Lvl " + details.min_level;
                                    } else if (details.min_level > 0 && cond15 != null) {
                                        cond16 = "Lvl " + details.min_level;
                                    }
                                    if (details.known_move != null && cond15 == null) {
                                        cond15 = "Knowing " + details.known_move.name;
                                    } else if (details.known_move != null && cond15 != null) {
                                        cond16 = "Knowing " + details.known_move.name;
                                    }
                                    if (details.needs_overworld_rain != false && cond15 == null) {
                                        cond15 = "Needs Overworld Rain";
                                    } else if (details.needs_overworld_rain != false && cond15 != null) {
                                        cond16 = "Needs Overworld Rain";
                                    }
                                    if (details.party_species != null && cond15 == null) {
                                        cond15 = "With " + details.party_species.name + " in the team";
                                    } else if (details.party_species != null && cond15 != null) {
                                        cond16 = "With " + details.party_species.name + "in the team";
                                    }
                                    if (details.party_type != null && cond15 == null) {
                                        cond15 = "With a type " + details.party_type.name + " in the team";
                                    } else if (details.party_type != null && cond15 != null) {
                                        cond16 = "With a type " + details.party_type.name + " in the team";
                                    }
                                    if (details.relative_physical_stats == 1 && cond15 == null) {
                                        cond15 = "With more Attack than Defense";
                                    } else if (details.relative_physical_stats == 1 && cond15 != null) {
                                        cond16 = "With more Attack than Defense";
                                    }
                                    if (details.relative_physical_stats == -1 && cond15 == null) {
                                        cond15 = "With more Defense than Attack";
                                    } else if (details.relative_physical_stats == -1 && cond15 != null) {
                                        cond16 = "With more Defense than Attack";
                                    }
                                    if (details.relative_physical_stats == 0 && cond15 == null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond15 = "With even Attack and Defense";
                                    } else if (details.relative_physical_stats == 0 && cond15 != null && evolves_toes.species.name.equals("hitmontop")) {
                                        cond16 = "With even Attack and Defense";
                                    }
                                    if (details.time_of_day.equals("day") && cond15 == null) {
                                        cond15 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("day") && cond15 != null) {
                                        cond16 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("dusk") && cond15 == null) {
                                        cond15 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("dusk") && cond15 != null) {
                                        cond16 = "At " + details.time_of_day;
                                    }
                                    if (details.time_of_day.equals("night") && cond15 == null) {
                                        cond15 = "At " + details.time_of_day;
                                    } else if (details.time_of_day.equals("night") && cond15 != null) {
                                        cond16 = "At " + details.time_of_day;
                                    }
                                    if (details.trade_species != null && cond15 == null) {
                                        cond15 = "With " + details.trade_species.name;
                                    } else if (details.trade_species != null && cond15 != null) {
                                        cond16 = "With " + details.trade_species.name;
                                    }
                                    if (details.turn_uspide_down != false && cond15 == null) {
                                        cond15 = "Turning console Upside Down";
                                    } else if (details.turn_uspide_down != false && cond15 != null) {
                                        cond16 = "Turning console Upside Down";
                                    }
                                }
                            }
                        }catch (Exception e) {
                        }try{


                            //obtener texto trigger


                            if(evolves_toes.evolves_to.size()==1){
                                thirdPokeChainAlt.setVisibility(View.GONE);
                                thirdPokeNameAlt.setVisibility(View.GONE);
                                trigger1.setVisibility(View.GONE);
                                condicion1rstEvo9.setVisibility(View.GONE);
                                condicion2ndaEvo9.setVisibility(View.GONE);
                            }else if(evolves_toes.evolves_to.size()==0||evolves_toes.evolves_to.isEmpty()){

                                thirdPokeChainAlt.setVisibility(View.GONE);
                                thirdPokeChain.setVisibility(View.GONE);
                                thirdPokeNameAlt.setVisibility(View.GONE);
                                thirsPokeName.setVisibility(View.GONE);
                                trigger9.setVisibility(View.GONE);
                                trigger1.setVisibility(View.GONE);
                                condicion1rstEvo8.setVisibility(View.GONE);
                                condicion1rstEvo9.setVisibility(View.GONE);
                                condicion2ndaEvo8.setVisibility(View.GONE);
                                condicion2ndaEvo9.setVisibility(View.GONE);
                            }
                            int counter2=0;

                            for(Evolves_to evolves_toss : evolves_toes.evolves_to){
                                /*if(evolves_toss.evolves_to.size()==1){
                                    thirdPokeChainAlt.setVisibility(View.GONE);
                                    thirdPokeNameAlt.setVisibility(View.GONE);
                                    trigger1.setVisibility(View.GONE);
                                    condicion1rstEvo9.setVisibility(View.GONE);
                                    condicion2ndaEvo9.setVisibility(View.GONE);
                                }else if(evolves_toss.evolves_to.size()==0||evolves_toss.evolves_to.isEmpty()){

                                    thirdPokeChainAlt.setVisibility(View.GONE);
                                    thirdPokeChain.setVisibility(View.GONE);
                                    thirdPokeNameAlt.setVisibility(View.GONE);
                                    thirsPokeName.setVisibility(View.GONE);
                                    trigger9.setVisibility(View.GONE);
                                    trigger1.setVisibility(View.GONE);
                                    condicion1rstEvo8.setVisibility(View.GONE);
                                    condicion1rstEvo9.setVisibility(View.GONE);
                                    condicion2ndaEvo8.setVisibility(View.GONE);
                                    condicion2ndaEvo9.setVisibility(View.GONE);
                                }    */
                                if(counter2==0) {

                                    url3rdEvo = evolves_toss.species.url;
                                    for(Evolution_Details details : evolves_toss.evolution_details){
                                        triggerFin1 = details.trigger.name;
                                        if(details.held_item != null&&condFin1==null){
                                            condFin1 = details.held_item.name;
                                        }else if(details.held_item!=null&&condFin1!=null){
                                            condFin2 = details.held_item.name;
                                        }
                                        if(details.gender == 1&&condFin1==null){
                                            condFin1 = "Female";
                                        }else if(details.gender==1&&condFin1!=null){
                                            condFin2 = "Female";
                                        }
                                        if(details.item != null&&condFin1==null){
                                            condFin1 = details.item.name;
                                        }else if(details.item!=null&&condFin1!=null){
                                            condFin2 = details.item.name;
                                        }
                                        if(details.known_move != null&&condFin1==null){
                                            condFin1 = "Knowing "+details.known_move.name;
                                        }else if(details.known_move!=null&&condFin1!=null){
                                            condFin2 = "Knowing "+details.known_move.name;
                                        }
                                        if(details.known_move_type != null&&condFin1==null){
                                            condFin1 = "Knowing "+details.known_move_type.name+" move";
                                        }else if(details.known_move!=null&&condFin1!=null){
                                            condFin2 = "Knowing "+details.known_move_type.name+" move";
                                        }
                                        if(details.location != null&&condFin1==null){
                                            condFin1 = "In "+details.location.name;
                                        }else if(details.location!=null&&condFin1!=null){
                                            condFin2 = "In "+details.location.name;
                                        }
                                        if(details.min_happiness>0 &&condFin1==null){
                                            condFin1 = "Min. Happiness "+details.min_happiness;
                                        }else if(details.min_happiness>0&&condFin1!=null){
                                            condFin2 = "Min. Happiness "+details.min_happiness;
                                        }
                                        if(details.min_beauty >0&&condFin1==null){
                                            condFin1 = "Min. Beauty "+details.min_beauty;
                                        }else if(details.min_beauty>0&&condFin1!=null){
                                            condFin2 = "Min.Beauty "+details.min_beauty;
                                        }
                                        if(details.min_level > 0 &&condFin1==null){
                                            condFin1 = "Lvl "+details.min_level;
                                        }else if(details.min_level>0&&condFin1!=null){
                                            condFin2 = "Lvl "+details.min_level;
                                        }
                                        if(details.known_move != null&&condFin1==null){
                                            condFin1 = "Knowing "+details.known_move.name;
                                        }else if(details.known_move!=null&&condFin1!=null){
                                            condFin2 = "Knowing "+details.known_move.name;
                                        }
                                        if(details.needs_overworld_rain != false&&condFin1==null){
                                            condFin1 = "Needs Overworld Rain";
                                        }else if(details.needs_overworld_rain!=false&&condFin1!=null){
                                            condFin2 = "Needs Overworld Rain";
                                        }
                                        if(details.party_species != null&&condFin1==null){
                                            condFin1 = "With "+details.party_species.name +" in the team";
                                        }else if(details.party_species!=null&&condFin1!=null){
                                            condFin2 = "With "+details.party_species.name+ "in the team";
                                        }
                                        if(details.party_type != null&&condFin1==null){
                                            condFin1 = "With a type "+details.party_type.name+" in the team";
                                        }else if(details.party_type!=null&&condFin1!=null){
                                            condFin2 = "With a type "+details.party_type.name+" in the team";
                                        }
                                        if(details.relative_physical_stats ==1 &&condFin1==null){
                                            condFin1 = "With more Attack than Defense";
                                        }else if(details.relative_physical_stats==1&&condFin1!=null){
                                            condFin2 = "With more Attack than Defense";
                                        }
                                        if(details.relative_physical_stats ==-1 &&condFin1==null){
                                            condFin1 = "With more Defense than Attack";
                                        }else if(details.relative_physical_stats==-1&&condFin1!=null){
                                            condFin2 = "With more Defense than Attack";
                                        }
                                        if(details.relative_physical_stats ==0 &&condFin1==null&&evolves_toss.species.name.equals("hitmontop")){
                                            condFin1 = "With even Attack and Defense";
                                        }else if(details.relative_physical_stats==0&&condFin1!=null&&evolves_toss.species.name.equals("hitmontop")){
                                            condFin2 = "With even Attack and Defense";
                                        }
                                        if(details.time_of_day.equals("day") &&condFin1==null){
                                            condFin1 = "At "+details.time_of_day;
                                        }else if(details.time_of_day.equals("day")&&condFin1!=null){
                                            condFin2 = "At "+details.time_of_day;
                                        }
                                        if(details.time_of_day.equals("dusk") &&condFin1==null){
                                            condFin1 = "At "+details.time_of_day;
                                        }else if(details.time_of_day.equals("dusk")&&condFin1!=null){
                                            condFin2 = "At "+details.time_of_day;
                                        }
                                        if(details.time_of_day.equals("night")&&condFin1==null){
                                            condFin1 = "At "+details.time_of_day;
                                        }else if(details.time_of_day.equals("night")&&condFin1!=null){
                                            condFin2 = "At "+details.time_of_day;
                                        }
                                        if(details.trade_species !=null&&condFin1==null){
                                            condFin1 = "With "+details.trade_species.name;
                                        }else if(details.trade_species!=null&&condFin1!=null){
                                            condFin2 = "With "+details.trade_species.name;
                                        }
                                        if(details.turn_uspide_down !=false&&condFin1==null){
                                            condFin1 = "Turning console Upside Down";
                                        }else if(details.turn_uspide_down!=false&&condFin1!=null){
                                            condFin2 = "Turning console Upside Down";
                                        }
                                    }
                                }else if(counter2==1){
                                    url3rdEvoAlt=evolves_toss.species.url;
                                    for(Evolution_Details details : evolves_toss.evolution_details){
                                        triggerFin2 = details.trigger.name;
                                        if(details.held_item != null&&condFin3==null){
                                            condFin3 = details.held_item.name;
                                        }else if(details.held_item!=null&&condFin3!=null){
                                            condFin4 = details.held_item.name;
                                        }
                                        if(details.gender == 1&&condFin3==null){
                                            condFin3 = "Female";
                                        }else if(details.gender==1&&condFin3!=null){
                                            condFin4 = "Female";
                                        }
                                        if(details.item != null&&condFin3==null){
                                            condFin3 = details.item.name;
                                        }else if(details.item!=null&&condFin3!=null){
                                            condFin4 = details.item.name;
                                        }
                                        if(details.known_move != null&&condFin3==null){
                                            condFin3 = "Knowing "+details.known_move.name;
                                        }else if(details.known_move!=null&&condFin3!=null){
                                            condFin4 = "Knowing "+details.known_move.name;
                                        }
                                        if(details.known_move_type != null&&condFin3==null){
                                            condFin3 = "Knowing "+details.known_move_type.name+" move";
                                        }else if(details.known_move!=null&&condFin3!=null){
                                            condFin4 = "Knowing "+details.known_move_type.name+" move";
                                        }
                                        if(details.location != null&&condFin3==null){
                                            condFin3 = "In "+details.location.name;
                                        }else if(details.location!=null&&condFin3!=null){
                                            condFin4 = "In "+details.location.name;
                                        }
                                        if(details.min_happiness>0 &&condFin3==null){
                                            condFin3 = "Min. Happiness "+details.min_happiness;
                                        }else if(details.min_happiness>0&&condFin3!=null){
                                            condFin4 = "Min. Happiness "+details.min_happiness;
                                        }
                                        if(details.min_beauty >0&&condFin3==null){
                                            condFin3 = "Min. Beauty "+details.min_beauty;
                                        }else if(details.min_beauty>0&&condFin3!=null){
                                            condFin4 = "Min.Beauty "+details.min_beauty;
                                        }
                                        if(details.min_level > 0 &&condFin3==null){
                                            condFin3 = "Lvl "+details.min_level;
                                        }else if(details.min_level>0&&condFin3!=null){
                                            condFin4 = "Lvl "+details.min_level;
                                        }
                                        if(details.known_move != null&&condFin3==null){
                                            condFin3 = "Knowing "+details.known_move.name;
                                        }else if(details.known_move!=null&&condFin3!=null){
                                            condFin4 = "Knowing "+details.known_move.name;
                                        }
                                        if(details.needs_overworld_rain != false&&condFin3==null){
                                            condFin3 = "Needs Overworld Rain";
                                        }else if(details.needs_overworld_rain!=false&&condFin3!=null){
                                            condFin4 = "Needs Overworld Rain";
                                        }
                                        if(details.party_species != null&&condFin3==null){
                                            condFin3 = "With "+details.party_species.name +" in the team";
                                        }else if(details.party_species!=null&&condFin3!=null){
                                            condFin4 = "With "+details.party_species.name+ "in the team";
                                        }
                                        if(details.party_type != null&&condFin3==null){
                                            condFin3 = "With a type "+details.party_type.name+" in the team";
                                        }else if(details.party_type!=null&&condFin3!=null){
                                            condFin4 = "With a type "+details.party_type.name+" in the team";
                                        }
                                        if(details.relative_physical_stats ==1 &&condFin3==null){
                                            condFin3 = "With more Attack than Defense";
                                        }else if(details.relative_physical_stats==1&&condFin3!=null){
                                            condFin4 = "With more Attack than Defense";
                                        }
                                        if(details.relative_physical_stats ==-1 &&condFin3==null){
                                            condFin3 = "With more Defense than Attack";
                                        }else if(details.relative_physical_stats==-1&&condFin3!=null){
                                            condFin4 = "With more Defense than Attack";
                                        }
                                        if(details.relative_physical_stats ==0 &&condFin3==null&&evolves_toss.species.name.equals("hitmontop")){
                                            condFin3 = "With even Attack and Defense";
                                        }else if(details.relative_physical_stats==0&&condFin3!=null&&evolves_toss.species.name.equals("hitmontop")){
                                            condFin4 = "With even Attack and Defense";
                                        }
                                        if(details.time_of_day.equals("day") &&condFin3==null){
                                            condFin3 = "At "+details.time_of_day;
                                        }else if(details.time_of_day.equals("day")&&condFin3!=null){
                                            condFin4 = "At "+details.time_of_day;
                                        }
                                        if(details.time_of_day.equals("dusk") &&condFin3==null){
                                            condFin3 = "At "+details.time_of_day;
                                        }else if(details.time_of_day.equals("dusk")&&condFin3!=null){
                                            condFin4 = "At "+details.time_of_day;
                                        }
                                        if(details.time_of_day.equals("night")&&condFin3==null){
                                            condFin3 = "At "+details.time_of_day;
                                        }else if(details.time_of_day.equals("night")&&condFin3!=null){
                                            condFin4 = "At "+details.time_of_day;
                                        }
                                        if(details.trade_species !=null&&condFin3==null){
                                            condFin3 = "With "+details.trade_species.name;
                                        }else if(details.trade_species!=null&&condFin3!=null){
                                            condFin4 = "With "+details.trade_species.name;
                                        }
                                        if(details.turn_uspide_down !=false&&condFin3==null){
                                            condFin3 = "Turning console Upside Down";
                                        }else if(details.turn_uspide_down!=false&&condFin3!=null){
                                            condFin4 = "Turning console Upside Down";
                                        }

                                    }
                                }
                                counter2++;
                            }
                        }catch (Exception e){
                            System.out.println("PATATA");
                        }
                        counter++;
                    }








                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            evolutionChain.chain.species.url,
                            null,
                            response2 ->{
                                JSONObject obj1 = response2;
                                specie = new GsonBuilder().create().fromJson(obj1.toString(),Specie.class);
                                idForImages = specie.id;


                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForImages+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(firstPokeChain);
                                            String str = pokemon.name;
                                            nameFirstChain.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())



                    ));

                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url2ndPoke,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo1 = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idForEvoImage = specieevo1.id;

                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForEvoImage+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(secondPokeChain);
                                            String str = pokemon.name;
                                            secondPokeName.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger.setText(triggero1);
                                            condicion1rstEvo.setText(cond1);
                                            if(cond2!=null){
                                                condicion2ndaEvo.setText(cond2);
                                            }else{
                                                condicion2ndaEvo.setVisibility(View.GONE);
                                            }


                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));
                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url2ndPokeAlt,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo1Alt = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idForEvoImageAlt = specieevo1Alt.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForEvoImageAlt+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(secondPokeChain2);
                                            String str = pokemon.name;
                                            secondPokeName2.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger2.setText(triggero2);
                                            condicion1rstEvo1.setText(cond3);
                                            if(cond4!=null){
                                                condicion2ndaEvo1.setText(cond4);
                                            }else{
                                                condicion2ndaEvo1.setVisibility(View.GONE);
                                            }


                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));
                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url2ndPokeAlt1,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo1Alt1 = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idForEvoImageAlt1 = specieevo1Alt1.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForEvoImageAlt1+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(secondPokeChain3);
                                            String str = pokemon.name;
                                            secondPokeName3.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger3.setText(triggero3);
                                            condicion1rstEvo2.setText(cond5);
                                            if(cond6!=null){
                                                condicion2ndaEvo2.setText(cond6);
                                            }else{
                                                condicion2ndaEvo2.setVisibility(View.GONE);
                                            }
                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));
                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url2ndPokeAlt2,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo1Alt2 = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idForEvoImageAlt2 = specieevo1Alt2.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForEvoImageAlt2+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(secondPokeChain4);
                                            String str = pokemon.name;
                                            secondPokeName4.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger4.setText(triggero4);
                                            condicion1rstEvo3.setText(cond7);
                                            if(cond8!=null){
                                                condicion2ndaEvo3.setText(cond8);
                                            }else{
                                                condicion2ndaEvo3.setVisibility(View.GONE);
                                            }

                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));
                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url2ndPokeAlt3,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo1Alt3 = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idForEvoImageAlt3 = specieevo1Alt3.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForEvoImageAlt3+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(secondPokeChain5);
                                            String str = pokemon.name;
                                            secondPokeName5.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger5.setText(triggero5);
                                            condicion1rstEvo4.setText(cond9);
                                            if(cond10!=null){
                                                condicion2ndaEvo4.setText(cond10);
                                            }else{
                                                condicion2ndaEvo4.setVisibility(View.GONE);
                                            }

                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));
                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url2ndPokeAlt4,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo1Alt4 = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idForEvoImageAlt4 = specieevo1Alt4.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForEvoImageAlt4+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(secondPokeChain6);
                                            String str = pokemon.name;
                                            secondPokeName6.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger6.setText(triggero6);
                                            condicion1rstEvo5.setText(cond11);
                                            if(cond12!=null){
                                                condicion2ndaEvo5.setText(cond12);
                                            }else{
                                                condicion2ndaEvo5.setVisibility(View.GONE);
                                            }

                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));
                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url2ndPokeAlt5,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo1Alt5 = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idForEvoImageAlt5 = specieevo1Alt5.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForEvoImageAlt5+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(secondPokeChain7);
                                            String str = pokemon.name;
                                            secondPokeName7.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger7.setText(triggero7);
                                            condicion1rstEvo6.setText(cond13);
                                            if(cond14!=null){
                                                condicion2ndaEvo6.setText(cond14);
                                            }else{
                                                condicion2ndaEvo6.setVisibility(View.GONE);
                                            }

                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));
                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url2ndPokeAlt6,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo1Alt6 = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idForEvoImageAlt6 = specieevo1Alt6.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idForEvoImageAlt6+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(secondPokeChain8);
                                            String str = pokemon.name;
                                            secondPokeName8.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger8.setText(triggero8);
                                            condicion1rstEvo7.setText(cond15);
                                            if(cond16!=null){
                                                condicion2ndaEvo7.setText(cond16);
                                            }else{
                                                condicion2ndaEvo7.setVisibility(View.GONE);

                                            }

                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));




                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url3rdEvo,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo3 = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idFor3rdEvo = specieevo3.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idFor3rdEvo+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(thirdPokeChain);
                                            String str = pokemon.name;
                                            thirsPokeName.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger9.setText(triggerFin1);
                                            condicion1rstEvo8.setText(condFin1);
                                            if(condFin2!=null){
                                                condicion2ndaEvo8.setText(condFin2);
                                            }else{
                                                condicion2ndaEvo8.setVisibility(View.GONE);
                                            }

                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));
                    Volley.newRequestQueue(this).add(new JsonObjectRequest(
                            Request.Method.GET,
                            url3rdEvoAlt,
                            null,
                            response1 -> {
                                JSONObject secEvo = response1;
                                specieevo3Alt = new GsonBuilder().create().fromJson(secEvo.toString(),Specie.class);
                                idFor3rdEvoAlt = specieevo3Alt.id;
                                Volley.newRequestQueue(this).add(new JsonObjectRequest(
                                        Request.Method.GET,
                                        "https://pokeapi.co/api/v2/pokemon/"+idFor3rdEvoAlt+"/",
                                        null,
                                        response3 ->{
                                            JSONObject objImg = response3;
                                            pokemon = new GsonBuilder().create().fromJson(objImg.toString(),Pokemon.class);
                                            Picasso.get().load(pokemon.sprites.front_default).fit().centerCrop().into(thirdPokeChainAlt);
                                            String str = pokemon.name;
                                            thirdPokeNameAlt.setText(str.substring(0,1).toUpperCase()+str.substring(1));
                                            trigger1.setText(triggerFin2);
                                            condicion1rstEvo9.setText(condFin3);
                                            if(cond4!=null){
                                                condicion2ndaEvo9.setText(condFin4);
                                            }else{
                                                condicion2ndaEvo9.setVisibility(View.GONE);
                                            }

                                        },
                                        error ->      Log.d("tag","onErrorResponse: "+error.getMessage())



                                ));


                            },
                            error -> Log.d("tag","onErrorResponse: "+error.getMessage())




                    ));




                },
                error -> Log.d("tag","onErrorResponse; "+error.getMessage()+"es aqui")


        ));



    }



    private String getEsGenera(Specie sp1){
        String genero = " ";
        for(Genera g: sp1.genera){
            if(g.language.name.equals("es")){
                genero = g.genus;

            }
        }
        return genero;
    }

    private void typeValues(){
try {
    if (typeName1.equals("normal")) {
        type1Image.setImageResource(R.drawable.normal);
            /*pokonstraint.setBackgroundColor(getColor(R.color.white));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("fighting")) {
        type1Image.setImageResource(R.drawable.lucha);
            /*pokonstraint.setBackgroundColor(getColor(R.color.fight));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("flying")) {
        type1Image.setImageResource(R.drawable.volador);
            /*pokonstraint.setBackgroundColor(getColor(R.color.flight));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("poison")) {
        type1Image.setImageResource(R.drawable.veneno);
            /*pokonstraint.setBackgroundColor(getColor(R.color.poison));
            namePokemon.setTextColor(getColor(R.color.white));*/
    } else if (typeName1.equals("ground")) {
        type1Image.setImageResource(R.drawable.tierra);
            /*pokonstraint.setBackgroundColor(getColor(R.color.terra));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("rock")) {
        type1Image.setImageResource(R.drawable.roca);
            /*pokonstraint.setBackgroundColor(getColor(R.color.rock));
            namePokemon.setTextColor(getColor(R.color.white));*/
    } else if (typeName1.equals("bug")) {
        type1Image.setImageResource(R.drawable.bicho);
            /*pokonstraint.setBackgroundColor(getColor(R.color.bug));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("ghost")) {
        type1Image.setImageResource(R.drawable.fantasma);
            /*pokonstraint.setBackgroundColor(getColor(R.color.ghost));
            namePokemon.setTextColor(getColor(R.color.white));*/
    } else if (typeName1.equals("steel")) {
        type1Image.setImageResource(R.drawable.acero);
            /*pokonstraint.setBackgroundColor(getColor(R.color.steel));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("fire")) {
        type1Image.setImageResource(R.drawable.fuego);
            /*pokonstraint.setBackgroundColor(getColor(R.color.red));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("water")) {
        type1Image.setImageResource(R.drawable.agua);
            /*pokonstraint.setBackgroundColor(getColor(R.color.water));
            namePokemon.setTextColor(getColor(R.color.white));*/
    } else if (typeName1.equals("grass")) {
        type1Image.setImageResource(R.drawable.planta);
            /*pokonstraint.setBackgroundColor(getColor(R.color.grass));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("electric")) {
        type1Image.setImageResource(R.drawable.electrico);
            /*pokonstraint.setBackgroundColor(getColor(R.color.elec));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("psychic")) {
        type1Image.setImageResource(R.drawable.psiquico);
            /*pokonstraint.setBackgroundColor(getColor(R.color.psy));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("ice")) {
        type1Image.setImageResource(R.drawable.hielo);
            /*pokonstraint.setBackgroundColor(getColor(R.color.ice));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("dragon")) {
        type1Image.setImageResource(R.drawable.dragon);
            /*pokonstraint.setBackgroundColor(getColor(R.color.dragon));
            namePokemon.setTextColor(getColor(R.color.black));*/
    } else if (typeName1.equals("dark")) {
        type1Image.setImageResource(R.drawable.siniestro);
            /*pokonstraint.setBackgroundColor(getColor(R.color.black));
            namePokemon.setTextColor(getColor(R.color.white));*/
    } else if (typeName1.equals("fairy")) {
        type1Image.setImageResource(R.drawable.hada);
            /*pokonstraint.setBackgroundColor(getColor(R.color.hada));
            namePokemon.setTextColor(getColor(R.color.black));*/
    }
}catch (Exception e){

}

        try {
            System.out.println("hay2tipo");
            if (typeName2.equals("normal")) {
                type2Image.setImageResource(R.drawable.normal);
            } else if (typeName2.equals("fighting")) {
                type2Image.setImageResource(R.drawable.lucha);

            } else if (typeName2.equals("flying")) {
                type2Image.setImageResource(R.drawable.volador);

            } else if (typeName2.equals("poison")) {
                type2Image.setImageResource(R.drawable.veneno);

            } else if (typeName2.equals("ground")) {
                type2Image.setImageResource(R.drawable.tierra);

            } else if (typeName2.equals("rock")) {
                type2Image.setImageResource(R.drawable.roca);

            } else if (typeName2.equals("bug")) {
                type2Image.setImageResource(R.drawable.bicho);

            } else if (typeName2.equals("ghost")) {
                type2Image.setImageResource(R.drawable.fantasma);

            } else if (typeName2.equals("steel")) {
                type2Image.setImageResource(R.drawable.acero);

            } else if (typeName2.equals("fire")) {
                type2Image.setImageResource(R.drawable.fuego);

            } else if (typeName2.equals("water")) {
                type2Image.setImageResource(R.drawable.agua);

            } else if (typeName2.equals("grass")) {
                type2Image.setImageResource(R.drawable.planta);

            } else if (typeName2.equals("electric")) {
                type2Image.setImageResource(R.drawable.electrico);

            } else if (typeName2.equals("psychic")) {
                type2Image.setImageResource(R.drawable.psiquico);

            } else if (typeName2.equals("ice")) {
                type2Image.setImageResource(R.drawable.hielo);

            } else if (typeName2.equals("dragon")) {
                type2Image.setImageResource(R.drawable.dragon);

            } else if (typeName2.equals("dark")) {
                type2Image.setImageResource(R.drawable.siniestro);

            } else if (typeName2.equals("fairy")) {
                type2Image.setImageResource(R.drawable.hada);

            }
        }catch (Exception e){
            type2Image.setImageResource(0);
        }





    }


}