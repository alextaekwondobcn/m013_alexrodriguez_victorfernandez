package com.example.customdex_m013;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.customdex_m013.model.Comment;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;

public class ForumDetailActivity extends AppCompatActivity {

    private ImageView profilePic;
    private TextView premise;
    private TextView authorName;
    private ArrayList<Comment> commentList = new ArrayList<>();
    private ImageButton publishComment;
    private TextInputEditText writtenComment;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private CommentAdapter adapter;
    private RecyclerView recyclerView;

    String picUrl;
    String premiseDetail;
    String authorNamee;
    String forumId;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_detail);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        picUrl = getIntent().getStringExtra("url");
        premiseDetail = getIntent().getStringExtra("premise");
        authorNamee = getIntent().getStringExtra("authorName");
        forumId = getIntent().getStringExtra("forumId");

        profilePic = findViewById(R.id.profilePicDetail);
        premise = findViewById(R.id.premiseDetail);
        authorName = findViewById(R.id.authorNameDetail);
        publishComment = findViewById(R.id.publishComment);
        writtenComment = findViewById(R.id.comment);
        recyclerView = findViewById(R.id.comments);

        premise.setText(premiseDetail);
        Picasso.get().load(picUrl).fit().centerCrop().into(profilePic);
        authorName.setText(authorNamee);

        setQuery().addSnapshotListener((collectionSnapshot, e) -> {
            commentList.clear();
            for (DocumentSnapshot documentSnapshot: collectionSnapshot) {
                Comment comment = documentSnapshot.toObject(Comment.class);
                comment.commentId = documentSnapshot.getId();
                commentList.add(comment);
                Collections.sort(commentList, new SortByDate());
            }
            adapter.notifyDataSetChanged();
        });

        publishComment.setOnClickListener(publish -> {
            publishComment.setEnabled(false);
            Comment comment = new Comment();
            comment.authorName = String.valueOf(FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
            comment.content = writtenComment.getText().toString();
            comment.url = String.valueOf(FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl());
            comment.commentId = UUID.randomUUID().toString();
            comment.forumId = forumId;
            comment.date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));

            FirebaseFirestore.getInstance().collection("forums").document(forumId).collection("comments")
                    .add(comment)
                    .addOnCompleteListener(task -> {
                        publishComment.setEnabled(true);
                        writtenComment.setText("");
                    });
        });
//s
        recyclerView.setAdapter(adapter = new CommentAdapter(commentList, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    Query setQuery() { return db.collection("forums").document(forumId).collection("comments"); }

    class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder>{

        private ArrayList<Comment> commentArrayList;
        private Context context;

        public CommentAdapter(ArrayList<Comment> commentArrayList, Context context) {
            this.commentArrayList = commentArrayList;
            this.context = context;
        }
        @NonNull
        @Override
        public CommentAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.comment_row, parent, false );
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CommentAdapter.MyViewHolder holder, int position) {
            Picasso.get().load(commentArrayList.get(position).url).fit().centerCrop().into(holder.profilepic);
            holder.content.setText(commentArrayList.get(position).content);
            holder.name.setText(commentArrayList.get(position).authorName);
            holder.date.setText(commentArrayList.get(position).date);
        }

        @Override
        public int getItemCount() {
            return commentArrayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView name;
            private TextView content;
            private ImageView profilepic;
            private TextView date;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                name = itemView.findViewById(R.id.author);
                content = itemView.findViewById(R.id.content);
                profilepic = itemView.findViewById(R.id.authorImage);
                date = itemView.findViewById(R.id.date);
            }
        }
    }
    static class SortByDate implements Comparator<Comment> {

        @Override
        public int compare(Comment comment, Comment t1) {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            try {
                return format.parse(comment.date).compareTo(format.parse(t1.date));
            } catch (ParseException e) {
                e.printStackTrace();
                return 0;
            }
        }

        @Override
        public boolean equals(Object o) {
            return false;
        }
    }
}