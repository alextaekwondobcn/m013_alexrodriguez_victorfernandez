package com.example.customdex_m013;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.widget.Toolbar;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.customdex_m013.databinding.ActivityMainBinding;
import com.example.customdex_m013.model.FavoriteActivity;
import com.example.customdex_m013.model.ProfileActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    private BottomNavigationView nMainNav;
    private FrameLayout nMainFrame;
    private GenerationFragment generationFragment;
    private HomeFragment homeFragment;
    private AddPokemonFragment addPokemonFragment;
    Toolbar toolbar;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        setContentView((binding= ActivityMainBinding.inflate(getLayoutInflater())).getRoot());
        nMainNav = findViewById(R.id.main_nav);
        nMainFrame = findViewById(R.id.mainFrame);

        generationFragment = new GenerationFragment();
        homeFragment = new HomeFragment();
        addPokemonFragment = new AddPokemonFragment();
        setFragment(homeFragment);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setLogo(R.drawable.customdex);

        nMainNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.nav_home:
                        setFragment(homeFragment);
                        return true;
                    case R.id.addPokemon:
                        setFragment(addPokemonFragment);
                        return true;
                    case R.id.pokedex:
                        setFragment(generationFragment);
                        return true;
                    default:
                        return false;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.profile:
                Intent i = new Intent(this, ProfileActivity.class);
                startActivity(i);
                return true;
            case R.id.favoritos:
                Intent e = new Intent(this, FavoriteActivity.class);
                startActivity(e);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void setFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, fragment);
        fragmentTransaction.commit();
    }

}
